use std::{
	cmp::Ordering,
	ffi::CStr,
	marker::PhantomData,
	os::raw::{
		c_char,
		c_int,
	},
	ptr,
	slice,
};

use swi_prolog_sys::*;

use crate::{
	atom::{
		Atom,
		Functor,
	},
	exception::Exception,
	frame::{
		Frame,
		FrameSealed,
	},
	module::Module,
};

macro_rules! term_is_impl {
	($ident:ident, $func:ident) => {
		pub fn $ident(&self) -> bool {
			unsafe { return $func(self.term) != 0 }
		}
	};
}

/// Prolog term
///
/// Allocated on prolog's stack, so they have a lifetime.
///
/// Note that even if unify functions fail, they may still have bindings left over. USe a subframe to "try out" unifications and
/// undo bindings if they fail.
///
/// Most of these methods are obvious wrappers for their PL C counterparts.
#[derive(PartialEq, Eq, PartialOrd)]
#[repr(transparent)]
pub struct Term<'ctx> {
	// Note: because terms are allocated on prolog's stack, which is only available when a prolog engine is bound, there can't actually be
	// a reachable Term for a different prolog engine in existance on a thread, so we don't need to keep a reference to the engine.
	pub(crate) term: term_t,
	_lt: PhantomData<&'ctx ()>,
}
impl<'ctx> Term<'ctx> {
	term_is_impl!(is_variable, PL_is_variable);

	term_is_impl!(is_ground, PL_is_ground);

	term_is_impl!(is_atom, PL_is_atom);

	term_is_impl!(is_integer, PL_is_integer);

	term_is_impl!(is_string, PL_is_string);

	term_is_impl!(is_float, PL_is_float);

	term_is_impl!(is_rational, PL_is_rational);

	term_is_impl!(is_compound, PL_is_compound);

	term_is_impl!(is_callable, PL_is_callable);

	term_is_impl!(is_list, PL_is_list);

	term_is_impl!(is_dict, PL_is_dict);

	term_is_impl!(is_pair, PL_is_pair);

	term_is_impl!(is_atomic, PL_is_atomic);

	term_is_impl!(is_number, PL_is_number);

	term_is_impl!(is_acyclic, PL_is_acyclic);

	term_is_impl!(is_nil, PL_get_nil);

	pub(crate) unsafe fn take(term: term_t) -> Self {
		if term == 0 {
			panic!("Term allocation failed")
		}
		Self {
			term,
			_lt: Default::default(),
		}
	}

	pub fn type_of(&self) -> TermType {
		unsafe { TermType::from_c(PL_term_type(self.term)) }
	}

	pub fn is_functor(&self, functor: &Functor) -> bool {
		return unsafe { PL_is_functor(self.term, functor.0) } != 0;
	}

	pub fn as_atom(&self) -> Option<Atom> {
		unsafe {
			let mut atom = Default::default();
			if PL_get_atom(self.term, &mut atom) != 0 {
				Some(Atom::take(atom))
			} else {
				None
			}
		}
	}

	pub fn as_atom_cstr(&self) -> Option<&CStr> {
		unsafe {
			let mut cptr = ptr::null_mut();
			if PL_get_atom_chars(self.term, &mut cptr) != 0 {
				Some(CStr::from_ptr(cptr))
			} else {
				None
			}
		}
	}

	pub fn as_string(&self) -> Option<String> {
		unsafe {
			let mut chars = ptr::null_mut();
			let mut len = 0;
			if PL_get_string(self.term, &mut chars, &mut len) != 0 {
				let slice = slice::from_raw_parts(chars as *mut u8, len as usize);
				Some(String::from_utf8_lossy(slice).into_owned())
			} else {
				None
			}
		}
	}

	pub fn as_i32(&self) -> Option<i32> {
		unsafe {
			let mut v = 0;
			if PL_get_integer(self.term, &mut v) != 0 {
				Some(v)
			} else {
				None
			}
		}
	}

	pub fn as_i64(&self) -> Option<i64> {
		unsafe {
			let mut v = 0;
			if PL_get_int64(self.term, &mut v) != 0 {
				Some(v)
			} else {
				None
			}
		}
	}

	pub fn as_bool(&self) -> Option<bool> {
		unsafe {
			let mut v = 0;
			if PL_get_bool(self.term, &mut v) != 0 {
				Some(v != 0)
			} else {
				None
			}
		}
	}

	pub fn as_f64(&self) -> Option<f64> {
		unsafe {
			let mut v = 0.0;
			if PL_get_float(self.term, &mut v) != 0 {
				Some(v)
			} else {
				None
			}
		}
	}

	pub fn as_functor(&self) -> Option<Functor> {
		unsafe {
			let mut v = 0;
			if PL_get_functor(self.term, &mut v) != 0 {
				Some(Functor(v))
			} else {
				None
			}
		}
	}

	pub fn as_compound(&self) -> Option<(Atom, u32)> {
		unsafe {
			let mut a = 0;
			let mut r = 0;
			if PL_get_compound_name_arity(self.term, &mut a, &mut r) != 0 {
				Some((Atom::take(a), r as u32))
			} else {
				None
			}
		}
	}

	pub fn as_name_arity(&self) -> Option<(Atom, u32)> {
		unsafe {
			let mut a = 0;
			let mut r = 0;
			if PL_get_name_arity(self.term, &mut a, &mut r) != 0 {
				Some((Atom::take(a), r as u32))
			} else {
				None
			}
		}
	}

	pub fn get_list(&self, head: &Term, tail: &Term) -> bool {
		unsafe { PL_get_list(self.term, head.term, tail.term) != 0 }
	}

	pub fn get_list_head(&self, head: &Term) -> bool {
		unsafe { PL_get_head(self.term, head.term) != 0 }
	}

	pub fn get_list_tail(&self, tail: &Term) -> bool {
		unsafe { PL_get_tail(self.term, tail.term) != 0 }
	}

	pub fn get_arg(&self, index: usize, out: &Term) -> bool {
		unsafe { PL_get_arg(index as c_int, self.term, out.term) != 0 }
	}

	pub fn format(&self) -> Result<String, Exception> {
		self.format_cargs(CVT_ALL | CVT_WRITE)
	}

	pub fn format_canonical(&self) -> Result<String, Exception> {
		self.format_cargs(CVT_ALL | CVT_WRITE_CANONICAL)
	}

	fn format_cargs(&self, flags: i32) -> Result<String, Exception> {
		unsafe {
			let mut buf = ptr::null_mut();
			let mut len = 0;
			Exception::nonzero_or_exc(PL_get_nchars(
				self.term,
				&mut len,
				&mut buf,
				(flags | BUF_DISCARDABLE | REP_UTF8 | CVT_EXCEPTION) as u32,
			))?;
			Ok(
				String::from_utf8_lossy(slice::from_raw_parts(buf as *mut u8, len as usize))
					.into_owned(),
			)
		}
	}

	pub fn skip_list(&self, tail: Option<&Term>) -> Result<(ListType, usize), Exception> {
		unsafe {
			let mut len = 0;
			let ty = match PL_skip_list(self.term, tail.map(|t| t.term).unwrap_or(0), &mut len) {
				PL_LIST => ListType::List,
				PL_PARTIAL_LIST => ListType::PartialList,
				PL_CYCLIC_TERM => ListType::CyclicTerm,
				PL_NOT_A_LIST => ListType::NotList,
				v => {
					return Err(Exception::nonzero_or_exc(v).unwrap_err());
				}
			};
			Ok((ty, len as usize))
		}
	}

	pub fn put_variable(&self) {
		unsafe {
			PL_put_variable(self.term);
		}
	}

	pub fn put_term(&self, other: &Term) -> Result<(), Exception> {
		unsafe { Exception::nonzero_or_exc(PL_put_term(self.term, other.term)) }
	}

	pub fn put_atom(&self, atom: &Atom) {
		unsafe {
			PL_put_atom(self.term, atom.0);
		}
	}

	pub fn put_bool(&self, value: bool) {
		unsafe {
			PL_put_bool(self.term, if value { 1 } else { 0 });
		}
	}

	pub fn put_str(&self, ty: StringType, value: &str) -> Result<(), Exception> {
		unsafe {
			Exception::nonzero_or_exc(PL_put_chars(
				self.term,
				ty.to_c(),
				value.len(),
				value.as_ptr() as *const c_char,
			))
		}
	}

	pub fn put_i64(&self, value: i64) -> Result<(), Exception> {
		unsafe { Exception::nonzero_or_exc(PL_put_int64(self.term, value)) }
	}

	pub fn put_u64(&self, value: u64) -> Result<(), Exception> {
		unsafe { Exception::nonzero_or_exc(PL_put_uint64(self.term, value)) }
	}

	pub fn put_f64(&self, value: f64) -> Result<(), Exception> {
		unsafe { Exception::nonzero_or_exc(PL_put_float(self.term, value)) }
	}

	pub fn put_functor(&self, value: &Functor) -> Result<(), Exception> {
		unsafe { Exception::nonzero_or_exc(PL_put_functor(self.term, value.0)) }
	}

	pub fn put_list(&self) -> Result<(), Exception> {
		unsafe { Exception::nonzero_or_exc(PL_put_list(self.term)) }
	}

	pub fn put_nil(&self) -> Result<(), Exception> {
		unsafe { Exception::nonzero_or_exc(PL_put_nil(self.term)) }
	}

	pub fn cons_list(&self, head: &Term, tail: &Term) -> Result<(), Exception> {
		unsafe { Exception::nonzero_or_exc(PL_cons_list(self.term, head.term, tail.term)) }
	}

	pub fn cons_functor<'a, I, F>(
		&self,
		frame: &F,
		functor: &Functor,
		args: I,
	) -> Result<(), Exception>
	where
		F: Frame,
		I: IntoIterator,
		I::Item: AsRef<Term<'a>>,
		I::IntoIter: ExactSizeIterator,
	{
		frame.with_reset_frame(|inner| unsafe {
			let arity = functor.arity();
			let (t_args, len) = inner.copy_contiguous(args);
			if len != arity as usize {
				PL_reset_term_refs(t_args);
				panic!("Functor arity does not match arguments list");
			}
			let res = PL_cons_functor_v(self.term, functor.0, t_args);
			if res == 0 {
				let exc = Exception::take_pl_exception()
					.expect("PL_cons_functor_v failed without an exception");
				PL_reset_term_refs(t_args);
				Err(exc)
			} else {
				PL_reset_term_refs(t_args);
				Ok(())
			}
		})
	}

	/// Puts a dictionary in this term.
	pub fn put_dict<'a, F, K, V, I>(
		&self,
		frame: &'ctx F,
		tag: Option<&Atom>,
		contents: I,
	) -> Result<(), Exception>
	where
		F: Frame,
		K: AsRef<Atom>,
		V: AsRef<Term<'a>>,
		I: IntoIterator<Item = (K, V)>,
		I::IntoIter: ExactSizeIterator,
	{
		unsafe {
			frame.with_reset_frame(|inner| {
				let iter = contents.into_iter();
				let len = iter.len();

				if len == 0 {
					return Exception::nonzero_or_exc(PL_put_dict(
						self.term,
						tag.map(|a| a.0).unwrap_or(0),
						0,
						ptr::null_mut(),
						0,
					))
					.map_err(|e| e.into());
				}

				let t = inner.alloc_many(len).into_iter().next().unwrap().term;
				let mut atoms = Vec::with_capacity(len);
				for (i, (k, v)) in iter.take(len).enumerate() {
					Exception::nonzero_or_exc(PL_put_term(t + i as term_t, v.as_ref().term))?;
					atoms.push(k.as_ref().0);
				}
				assert_eq!(atoms.len(), len, "ExactSizeIter lied about its size.");
				Exception::nonzero_or_exc(PL_put_dict(
					self.term,
					tag.map(|a| a.0).unwrap_or(0),
					len,
					atoms.as_ptr(),
					t,
				))?;
				Ok(())
			})
		}
	}

	pub fn unify_term(&self, other: &Term) -> Result<bool, Exception> {
		unsafe { Exception::bool_or_exc(PL_unify(self.term, other.term)) }
	}

	pub fn unify_atom(&self, atom: &Atom) -> Result<bool, Exception> {
		unsafe { Exception::bool_or_exc(PL_unify(self.term, atom.0)) }
	}

	pub fn unify_bool(&self, value: bool) -> Result<bool, Exception> {
		unsafe { Exception::bool_or_exc(PL_unify_bool(self.term, if value { 1 } else { 0 })) }
	}

	pub fn unify_i64(&self, value: i64) -> Result<bool, Exception> {
		unsafe { Exception::bool_or_exc(PL_unify_int64(self.term, value)) }
	}

	pub fn unify_u64(&self, value: u64) -> Result<bool, Exception> {
		unsafe { Exception::bool_or_exc(PL_unify_uint64(self.term, value)) }
	}

	pub fn unify_f64(&self, value: f64) -> Result<bool, Exception> {
		unsafe { Exception::bool_or_exc(PL_unify_float(self.term, value)) }
	}

	pub fn unify_functor(&self, value: &Functor) -> Result<bool, Exception> {
		unsafe { Exception::bool_or_exc(PL_unify_functor(self.term, value.0)) }
	}

	pub fn unify_compound(&self, value: &Functor) -> Result<bool, Exception> {
		unsafe { Exception::bool_or_exc(PL_unify_compound(self.term, value.0)) }
	}

	pub fn unify_list(&self, head: &Term, tail: &Term) -> Result<bool, Exception> {
		unsafe { Exception::bool_or_exc(PL_unify_list(self.term, head.term, tail.term)) }
	}

	pub fn unify_nil(&self) -> Result<bool, Exception> {
		unsafe { Exception::bool_or_exc(PL_unify_nil(self.term)) }
	}

	pub fn unify_arg(&self, index: i32, value: &Term) -> Result<bool, Exception> {
		unsafe { Exception::bool_or_exc(PL_unify_arg(index, self.term, value.term)) }
	}

	pub fn unify_str(&self, typ: StringType, value: &str) -> Result<bool, Exception> {
		unsafe {
			Exception::bool_or_exc(PL_unify_chars(
				self.term,
				typ.to_c() | REP_UTF8,
				value.len(),
				value.as_ptr() as *const c_char,
			))
		}
	}

	/// PL_put_term_from_chars
	pub fn parse(&self, s: &str) -> Result<(), Exception> {
		unsafe {
			Exception::nonzero_or_exc(PL_put_term_from_chars(
				self.term,
				REP_UTF8 | CVT_EXCEPTION,
				s.len(),
				s.as_ptr() as *const c_char,
			))
		}
	}

	/// Compares terms using their standard order (See `PL_compare`, `compare/3`)
	pub fn compare(&self, other: &Term) -> Ordering {
		unsafe {
			let v = PL_compare(self.term, other.term);
			if v < 0 {
				Ordering::Less
			} else if v == 0 {
				Ordering::Equal
			} else {
				Ordering::Greater
			}
		}
	}

	pub fn same_compound(&self, other: &Term) -> bool {
		unsafe { PL_same_compound(self.term, other.term) != 0 }
	}

	pub fn strip_module(&self, plain: &Term) -> Result<Option<Module>, Exception> {
		unsafe {
			let mut pmod = ptr::null_mut();
			Exception::nonzero_or_exc(PL_strip_module(self.term, &mut pmod, plain.term))?;
			if !pmod.is_null() {
				Ok(Some(Module(pmod)))
			} else {
				Ok(None)
			}
		}
	}
}
impl<'ctx> std::fmt::Debug for Term<'ctx> {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		f.debug_tuple("Term").field(&self.term).finish()
	}
}
impl<'ctx> AsRef<Term<'ctx>> for Term<'ctx> {
	fn as_ref(&self) -> &Term<'ctx> {
		self
	}
}

/// Result of `Term::type_of` indicating the type of the term.
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum TermType {
	Variable,
	Atom,
	Nil,
	Blob,
	String,
	Integer,
	Rational,
	Float,
	Term,
	ListPair,
	Dict,
}
impl TermType {
	pub(crate) fn from_c(v: i32) -> Self {
		match v {
			PL_VARIABLE => Self::Variable,
			PL_ATOM => Self::Atom,
			PL_NIL => Self::Nil,
			PL_BLOB => Self::Blob,
			PL_STRING => Self::String,
			PL_INTEGER => Self::Integer,
			PL_RATIONAL => Self::Rational,
			PL_FLOAT => Self::Float,
			PL_TERM => Self::Term,
			PL_LIST_PAIR => Self::ListPair,
			PL_DICT => Self::Dict,
			_ => panic!("Unrecognized term type: {}", v),
		}
	}
}

/// Result of `Term::skip_list` indicating list type
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum ListType {
	/// Proper list
	List,
	PartialList,
	CyclicTerm,
	NotList,
}

/// Type of string to generate
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum StringType {
	Atom,
	String,
	CodeList,
	CharList,
}
impl StringType {
	pub(crate) fn to_c(&self) -> c_int {
		match self {
			Self::Atom => PL_ATOM,
			Self::String => PL_STRING,
			Self::CodeList => PL_CODE_LIST,
			Self::CharList => PL_CHAR_LIST,
		}
	}
}
