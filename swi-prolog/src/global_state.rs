//! Internal global/thread-local state.

use std::{
	cell::RefCell,
	sync::Mutex,
};

use lazy_static::lazy_static;
use once_cell::sync::OnceCell;
use swi_prolog_sys::*;

/// Set when we know prolog has been initialized.
///
/// Lock SWIPL_INITTED_LOCK before setting!
static SWIPL_INITTED: OnceCell<()> = OnceCell::new();
lazy_static! {
	static ref SWIPL_INITTED_LOCK: Mutex<()> = Mutex::new(());
}

pub fn set_prolog_initted() {
	if SWIPL_INITTED.get().is_none() {
		let _lock = SWIPL_INITTED_LOCK.lock().unwrap();
		if SWIPL_INITTED.get().is_none() {
			SWIPL_INITTED.set(()).unwrap();
		}
	}
}

pub fn get_prolog_initted() -> bool {
	SWIPL_INITTED.get().is_some()
}

pub fn try_prolog_init<R, E, F: FnOnce() -> Result<R, E>>(cb: F) -> Option<Result<R, E>> {
	if SWIPL_INITTED.get().is_none() {
		let _lock = SWIPL_INITTED_LOCK.lock().unwrap();
		if SWIPL_INITTED.get().is_none() {
			return match (cb)() {
				Ok(v) => {
					SWIPL_INITTED.set(()).unwrap();
					Some(Ok(v))
				}
				Err(v) => Some(Err(v)),
			};
		}
	}
	None
}

pub fn check_prolog_initialized() {
	if !get_prolog_initted() {
		panic!(
			"SWIPL function that requires initialization called but hasn't been initialized yet."
		);
	}
}

#[derive(Default, Clone)]
struct StateBinding {
	exc_term: term_t,
}

thread_local! {
	static THREAD_STATE: RefCell<Option<StateBinding>> = RefCell::new(None);
}

/// Temporairly sets whether an engine is bound to the current thread, restoring the old value
/// when the returned value is dropped.
pub(crate) fn bind(exc_term: term_t) -> impl std::ops::Drop {
	let prev = THREAD_STATE.with(|state| {
		let mut state = state.borrow_mut();
		let prev = state.clone();
		*state = Some(StateBinding { exc_term });
		prev
	});

	scopeguard::guard((), move |()| {
		THREAD_STATE.with(|state| {
			let mut state = state.borrow_mut();
			*state = prev;
		});
	})
}

pub(crate) fn is_bound() -> bool {
	THREAD_STATE.with(|state| {
		let state = state.borrow();
		state.is_some()
	})
}

pub(crate) fn put_exception(e: term_t) {
	THREAD_STATE.with(|state| {
		let state = state.borrow();
		let state = state.as_ref().expect("No Prolog engine bound");
		unsafe {
			assert!(PL_put_term(state.exc_term, e) != 0, "PL_put_term failed");
		}
	})
}

pub(crate) fn get_exception() -> term_t {
	THREAD_STATE.with(|state| {
		let state = state.borrow();
		let state = state.as_ref().expect("No Prolog engine bound");
		state.exc_term
	})
}
