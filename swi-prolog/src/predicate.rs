use std::{
	ffi::CString,
	ptr,
};

use swi_prolog_sys::*;

use crate::{
	atom::{
		Atom,
		Functor,
	},
	global_state,
	module::Module,
};

#[derive(Debug, Clone)]
pub struct Predicate(pub(crate) predicate_t);
impl Predicate {
	pub fn get(id: &Functor, pmod: Option<&Module>) -> Predicate {
		global_state::check_prolog_initialized();
		unsafe { Self(PL_pred(id.0, pmod.map(|p| p.0).unwrap_or(ptr::null_mut()))) }
	}

	pub fn get_by_name_arity(name: &str, arity: u32, module: Option<&str>) -> Predicate {
		global_state::check_prolog_initialized();
		let cname = CString::new(name).expect("Predicate name contained null byte");
		let cmodule =
			module.map(|s| CString::new(s).expect("Predicate module name contained null byte"));
		unsafe {
			Self(PL_predicate(
				cname.as_ptr(),
				arity as i32,
				cmodule.as_ref().map(|s| s.as_ptr()).unwrap_or(ptr::null()),
			))
		}
	}

	pub fn info(&self) -> (Atom, u32, Module) {
		let mut atom = Default::default();
		let mut arity = Default::default();
		let mut pmod = ptr::null_mut();
		unsafe {
			PL_predicate_info(self.0, &mut atom, &mut arity, &mut pmod);
			(Atom::take(atom), arity as u32, Module(pmod))
		}
	}
}
