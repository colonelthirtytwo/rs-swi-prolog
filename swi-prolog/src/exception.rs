//! Prolog Exception Handling
//!
//! Unfortunately the Prolog API makes it hard to handle exceptions nicely.
//! It's a global value, and while one is set, we can't call a lot of Prolog API functions,
//! including cleanup functions like `PL_close_foreign_frame`. Additionally, we want to return
//! exceptions, possibly nested in other structures, as errors from inner foreign frames to
//! outer ones, which is complicated with Rust's current (as of this writing) lack of support
//! for generic associated types.
//!
//! End result: we create a thread-local global term to store the exception when entering a
//! Prolog context, and store the real exception in there, with the `Exception` type just
//! being a marker for "an exception happened".

use swi_prolog_sys::*;

use crate::{
	global_state,
	term::Term,
};

/// Marker that a Prolog exception should happen.
///
/// The actual exception term is in a thread-local global variable internal to the library.
/// See the module docs for more info.
#[derive(Debug)]
pub struct Exception;
impl Exception {
	/// Puts a custom exception in the global exception variable.
	pub fn put(exc: Term) -> Self {
		global_state::put_exception(exc.term);
		Self
	}

	/// Gets a reference to the exception term.
	pub fn term(&self) -> Term {
		unsafe { Term::take(global_state::get_exception()) }
	}

	/// Takes Prolog's exception, clears it, and stores it in our own exception state.
	pub(crate) unsafe fn take_pl_exception() -> Option<Self> {
		let t = PL_exception(0);
		if t != 0 {
			global_state::put_exception(t);
			PL_clear_exception();
			Some(Self)
		} else {
			None
		}
	}

	pub(crate) unsafe fn take_pl_exception_query(qid: qid_t) -> Option<Self> {
		let t = PL_exception(qid);
		if t != 0 {
			global_state::put_exception(t);
			Some(Self)
		} else {
			None
		}
	}

	/// Checks res. If nonzero, return Ok. Otherwise take the Prolog exception.
	///
	/// If zero and there was no exception, panic.
	pub(crate) unsafe fn nonzero_or_exc(res: i32) -> Result<(), Self> {
		if res != 0 {
			return Ok(());
		}
		let exc = Self::take_pl_exception()
			.expect("Prolog function returned error but no exception was set.");
		Err(exc)
	}

	/// If res is nonzero, return Ok(true). Otherwise check for an exception. If none, return Ok(false),
	/// otherwise take the exception and return Err
	pub(crate) unsafe fn bool_or_exc(res: i32) -> Result<bool, Self> {
		if res != 0 {
			Ok(true)
		} else if let Some(exc) = Self::take_pl_exception() {
			Err(exc)
		} else {
			Ok(false)
		}
	}
}
impl std::fmt::Display for Exception {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		// TODO: if this throws an exception, it will replace the current error. Should probably not do that, but what's the likelyhood of it actually failing?
		let s = self
			.term()
			.format_canonical()
			.map_err(|_| std::fmt::Error)?;
		f.write_str(&s)
	}
}
impl std::error::Error for Exception {}

/// Prolog "Error" - either a false result or an exception.
///
/// It's frequently convenient to exit a function ASAP on a "false" result, ex from unification.
/// This error type makes this ergonomic, letting you handle "false" results and exceptions using
/// Rust's Result mechanism.
pub enum Error {
	False,
	Exception(Exception),
}
impl<'a> From<Exception> for Error {
	fn from(v: Exception) -> Self {
		Self::Exception(v)
	}
}
impl<'a> std::fmt::Debug for Error {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		match self {
			Self::False => f.write_str("Prolog False"),
			Self::Exception(e) => std::fmt::Debug::fmt(e, f),
		}
	}
}
