use std::{
	cell::Cell,
	convert::TryInto,
	ffi::CString,
};

use smallvec::SmallVec;
use swi_prolog_sys::*;

use crate::{
	exception::Exception,
	term::Term,
};

/// Prolog calling context.
///
/// In existence only when a prolog engine is bound and in a state where it can be used.
/// This is how you allocate terms and check things about the engine.
///
/// All terms have a lifetime equal to this context, as terms are allocated on prolog's stack
/// and are released when the predicate returns.
///
/// You can create nested "subframes" from a frame: either Prolog "Foreign Frames" or a `ResetFrame`
/// wrapper for `PL_reset_term_refs`.
///
/// Do not use `std::mem::forget` or otherwise leak frames. For safety, it's required that all of the
/// frame's cleanup code run before returning to Prolog. If you do forget a frame, the library will
/// abort (not panic) the process.
pub unsafe trait Frame: FrameSealed {
	fn close_ok(self)
	where
		Self: Sized;
	fn close_err(self)
	where
		Self: Sized;

	/// Creates a new, unbound term.
	///
	/// Panics
	/// ======
	/// If no term could be allocated, or if this frame has a subframe that hasn't been dropped.
	fn alloc<'a>(&'a self) -> Term<'a> {
		self.check_no_subframe();

		let t = unsafe { PL_new_term_ref() };
		if t == 0 {
			panic!("PL_new_term_ref failed");
		}
		self.on_term_alloc(t, 1);
		unsafe { Term::take(t) }
	}
	/// Creates an array of new, unbound terms term with contiguous identifiers.
	///
	/// Panics
	/// ======
	/// If no term could be allocated, or if this frame has a subframe that hasn't been dropped.
	fn alloc_many<'a>(&'a self, num: usize) -> SmallVec<[Term<'a>; 16]> {
		self.check_no_subframe();

		let inum = num
			.try_into()
			.expect("Can't allocate that may terms at a time");
		let t = unsafe { PL_new_term_refs(inum) };
		if t == 0 {
			panic!("PL_new_term_refs failed");
		}
		self.on_term_alloc(t, num);
		(0..num)
			.map(|v| unsafe { Term::take(t + v as usize) })
			.collect()
	}
	/// Copies a term.
	///
	/// Panics
	/// ======
	/// If no term could be allocated, or if this frame has a subframe that hasn't been dropped.
	fn copy<'a>(&'a self, other: &Term) -> Term<'a> {
		self.check_no_subframe();

		let t = unsafe { PL_copy_term_ref(other.term) };
		if t == 0 {
			panic!("PL_copy_term_ref failed");
		}
		//self.frame_type.update_first(t);
		unsafe { Term::take(t) }
	}

	/// Creates a foreign frame.
	///
	/// TODO: If I'm reading the docs right, foreign frames aren't useful unless you can
	/// open queries in them, which we can't do yet.
	fn start_foreign_frame<'parent>(&'parent self) -> ForeignFrame<'parent, Self> {
		self.check_no_subframe();
		let fid = unsafe { PL_open_foreign_frame() };
		if fid == 0 {
			panic!("PL_open_foreign_frame returned 0");
		}
		self.subframe_opened();
		ForeignFrame {
			parent: self,
			fid,
			has_subframe: Cell::new(false),
		}
	}

	/// Creates a reset frame.
	///
	/// Terms allocated from this frame will be freed when it is dropped, but you won't be able to
	/// use the "parent" frame while the subframe has not been dropped.
	///
	/// Use this to free allocations in long-running procedures.
	///
	/// This is a safe wrapper around `PL_reset_term_refs`.
	fn start_reset_frame<'parent>(&'parent self) -> ResetFrame<'parent, Self> {
		self.check_no_subframe();
		self.subframe_opened();
		unsafe { ResetFrame::start(self) }
	}

	/// Executes a function in the context of a reset frame.
	fn with_reset_frame<R, E, F>(&self, exec: F) -> Result<R, E>
	where
		F: for<'sub> FnOnce(&'sub ResetFrame<Self>) -> Result<R, E>,
	{
		self.check_no_subframe();
		let frame = self.start_reset_frame();
		match (exec)(&frame) {
			Ok(v) => {
				frame.close_ok();
				Ok(v)
			}
			Err(v) => {
				frame.close_err();
				Err(v)
			}
		}
	}

	/// Creates a type error
	fn type_error<'a>(&'a self, expected: &str, culprit: &Term) -> Exception {
		self.check_no_subframe();

		let cstr = CString::new(expected).unwrap();
		unsafe {
			Exception::nonzero_or_exc(PL_type_error(cstr.as_ptr(), culprit.term))
				.err()
				.unwrap()
		}
	}
}

mod frame_sealed {
	use super::*;

	/// Private frame methods
	pub unsafe trait FrameSealed {
		/// Gets the subframe cell, only for the default implementations of methods in this trait.
		unsafe fn get_has_subframe_cell(&self) -> &Cell<bool>;

		/// Called on term(s) allocation
		fn on_term_alloc(&self, _term: term_t, _num: usize) {}

		/// Copies an iterable of terms into a contiguous span.
		///
		/// Several PL API functions take a span of contiguous terms. This function sets that up.
		///
		/// If the iterator has zero items, returns `(0,0)`.
		unsafe fn copy_contiguous<'a, I>(&self, iter: I) -> (term_t, usize)
		where
			I: IntoIterator,
			I::Item: AsRef<Term<'a>>,
			I::IntoIter: ExactSizeIterator,
		{
			let mut iter = iter.into_iter();
			let len = iter.len();
			if len == 0 {
				debug_assert!(iter.next().is_none(), "ExactSizeIterator lied about length");
				return (0, 0);
			}

			let t_begin = PL_new_term_refs(len.try_into().expect("Too many arguments"));
			if t_begin == 0 {
				panic!("PL_new_term_refs failed");
			}
			self.on_term_alloc(t_begin, len);

			let mut ctr = 0usize;
			for (i, t) in iter.enumerate() {
				if i >= len {
					panic!("ExactSizeIterator lied about length")
				}
				assert!(
					PL_put_term(t_begin + i as term_t, t.as_ref().term) != 0,
					"PL_put_term failed"
				);
				ctr += 1;
			}
			if ctr != len {
				panic!("ExactSizeIterator lied about length")
			}

			(t_begin, len)
		}

		/// Panics if a subframe exists.
		///
		/// Call before any method.
		fn check_no_subframe(&self) {
			unsafe {
				if self.get_has_subframe_cell().get() {
					panic!("cannot call functions on this frame while a subframe exists")
				}
			}
		}

		/// Notifies the frame that a subframe has been created.
		fn subframe_opened(&self) {
			unsafe {
				let c = self.get_has_subframe_cell();
				debug_assert!(!c.get());
				c.set(true);
			}
		}

		/// Notifies the frame that the subframe has been dropped.
		unsafe fn subframe_closed(&self) {
			let c = self.get_has_subframe_cell();
			debug_assert!(c.get());
			c.set(false);
		}

		/// Aborts if a subframe exists. Call this on drop.
		fn abort_no_subframe(&self) {
			unsafe {
				if self.get_has_subframe_cell().get() {
					eprintln!("Dropped a frame while a subframe exists. The programmer probably `std:mem::forget`'d it. This breaks the prolog stack, aborting.");
					std::process::abort();
				}
			}
		}
	}
}
pub(crate) use frame_sealed::FrameSealed;

pub struct ForeignFrame<'parent, Parent>
where
	Parent: Frame + ?Sized,
{
	parent: &'parent Parent,
	fid: fid_t,
	has_subframe: Cell<bool>,
}
unsafe impl<'parent, Parent> Frame for ForeignFrame<'parent, Parent>
where
	Parent: Frame + ?Sized,
{
	fn close_ok(self)
	where
		Self: Sized,
	{
		self.close();
	}

	fn close_err(self)
	where
		Self: Sized,
	{
		self.discard();
	}
}
unsafe impl<'parent, Parent> FrameSealed for ForeignFrame<'parent, Parent>
where
	Parent: Frame + ?Sized,
{
	unsafe fn get_has_subframe_cell(&self) -> &Cell<bool> {
		&self.has_subframe
	}
}
impl<'parent, Parent> ForeignFrame<'parent, Parent>
where
	Parent: Frame + ?Sized,
{
	/// Rewinds the foreign frame, undoing bindings and discarding terms.
	///
	/// Takes and returns the frame, to ensure that no terms that the frame
	/// created are still reachable.
	///
	/// Panics
	/// ======
	/// If the frame is a "root" frame, provided by `Engine::bind` or a prolog closure.
	pub fn rewind(self) -> Self {
		unsafe {
			PL_rewind_foreign_frame(self.fid);
		}
		self
	}

	/// Closes the foreign frame, committing bindings and discarding terms.
	pub fn close(mut self) {
		unsafe {
			PL_close_foreign_frame(self.fid);
		}
		self.fid = 0;
	}

	/// Discards the foreign frame, discarding bindings and terms.
	///
	/// Same as dropping the frame. Literally just drops it.
	pub fn discard(self) {
		std::mem::drop(self)
	}
}
impl<'parent, Parent> std::ops::Drop for ForeignFrame<'parent, Parent>
where
	Parent: Frame + ?Sized,
{
	fn drop(&mut self) {
		unsafe {
			self.abort_no_subframe();
			self.parent.subframe_closed();
			if self.fid == 0 {
				return;
			}
			PL_close_foreign_frame(self.fid);
		}
	}
}

/// Frame that clears all allocated terms when closed or dropped.
///
/// Uses `PL_reset_term_refs` to deallocate terms.
pub struct ResetFrame<'parent, Parent>
where
	Parent: Frame + ?Sized,
{
	parent: &'parent Parent,
	first: Cell<Option<term_t>>,
	has_subframe: Cell<bool>,
}
impl<'parent, Parent> ResetFrame<'parent, Parent>
where
	Parent: Frame + ?Sized,
{
	pub(crate) unsafe fn start(p: &'parent Parent) -> Self {
		Self {
			parent: p,
			first: Cell::new(None),
			has_subframe: Cell::new(false),
		}
	}
}
unsafe impl<'parent, Parent> Frame for ResetFrame<'parent, Parent>
where
	Parent: Frame + ?Sized,
{
	fn close_ok(self)
	where
		Self: Sized,
	{
	}

	fn close_err(self)
	where
		Self: Sized,
	{
	}
}
unsafe impl<'parent, Parent> FrameSealed for ResetFrame<'parent, Parent>
where
	Parent: Frame + ?Sized,
{
	unsafe fn get_has_subframe_cell(&self) -> &Cell<bool> {
		&self.has_subframe
	}

	fn on_term_alloc(&self, term: term_t, _: usize) {
		if self.first.get().is_none() {
			self.first.set(Some(term));
		}
	}
}
impl<'parent, Parent> std::ops::Drop for ResetFrame<'parent, Parent>
where
	Parent: Frame + ?Sized,
{
	fn drop(&mut self) {
		unsafe {
			self.abort_no_subframe();
			self.parent.subframe_closed();
			if let Some(t) = self.first.get() {
				PL_reset_term_refs(t);
			}
		}
	}
}
