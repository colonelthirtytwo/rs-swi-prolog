//! Library for safe access to SWI-Prolog.
//!
//! Supports both embedding the SWI-Prolog engine (see `Engine`) or exposing a foreign library (see `prolog_predicate` and `export_prolog_predicates`).
//!
//! Linking Methods
//! ===============
//!
//! Note that in both cases, SWI-Prolog will likely need its home directory set up with the Prolog libraries. Use the `qsave_compile`
//! infrastructure to avoid needing the libraries on the machine your program is running on.
//!
//! Dynamic Library with System Library
//! -----------------------------------
//! The default. During build, the library will search for the swipl shared object either by running `swipl` on the `PATH` to get
//! the prolog home directory, or by using the directory path in `RS_SWI_PL_HOME` environmental variable if defined.
//!
//! On some (most?) systems, the swipl library isn't in the normal library load paths, so using the built library will likely
//! result in "shared library not found" errors. Unfortunately this library can't do much about that issue; you can either set
//! `LD_LIBRARY_PATH` when running your program or use `cargo rustc` to add an rpath to your build.
//!
//! Static Linking with Crate-Built Library
//! ---------------------------------------
//! With the `vendored` feature, the swi-prolog-sys crate will download and build a copy of SWI-Prolog into a static library,
//! and link with that.

pub mod atom;
pub mod engine;
pub mod exception;
pub mod frame;
mod global_state;
pub mod module;
pub mod predicate;
pub mod predicate_export;
#[cfg(feature = "serde-fmt")]
pub mod serde;
pub mod term;

pub use swi_prolog_sys as sys;

pub mod prelude {
	pub use super::{
		atom::{
			Atom,
			Functor,
		},
		engine::{
			Engine,
			EngineFrame,
		},
		exception::{
			Error as PlError,
			Exception,
		},
		export_prolog_predicates,
		frame::Frame,
		module::Module,
		predicate::Predicate,
		predicate_export::PredicateFrame,
		prolog_nondet_predicate,
		prolog_predicate,
		term::{
			ListType,
			StringType,
			Term,
			TermType,
		},
	};
}
