use std::{
	cell::Cell,
	ffi::{
		c_void,
		CString,
	},
	fmt::Display,
	panic::{
		catch_unwind,
		AssertUnwindSafe,
	},
	ptr,
};

use smallvec::SmallVec;
use swi_prolog_sys::*;

use crate::{
	atom::Functor,
	exception::{
		Error,
		Exception,
	},
	frame::{
		Frame,
		FrameSealed,
	},
	global_state,
	term::{
		StringType,
		Term,
	},
};

/// Frame for prolog-called predicates
///
/// TODO: this can call queries, implement that, or just merge with EngineFrame
pub struct PredicateFrame {
	has_subframe: Cell<bool>,
}
impl PredicateFrame {
	pub unsafe fn new() -> Self {
		PredicateFrame {
			has_subframe: Cell::new(false),
		}
	}
}
unsafe impl Frame for PredicateFrame {
	fn close_ok(self)
	where
		Self: Sized,
	{
	}

	fn close_err(self)
	where
		Self: Sized,
	{
	}
}
unsafe impl FrameSealed for PredicateFrame {
	unsafe fn get_has_subframe_cell(&self) -> &Cell<bool> {
		&self.has_subframe
	}
}

/// Internal trait, exposed only for use with the predicate macros. Do not use.
///
/// Records meta-info and implementation about a prolog predicate.
#[doc(hidden)]
pub unsafe trait InternalPrologPredicate {
	/// The identifier's name, used as a default name
	const IDENT_NAME: &'static str;
	/// Is the predicate non-deterministic?
	const NONDET: bool;
	/// Predicate arity
	const ARITY: i32;
	/// Meta-predicate info string, if any
	const META: Option<&'static str>;
	/// Gets the prolog function
	fn predicate() -> unsafe extern "C" fn() -> foreign_t;
	/// Registers the module. Has a trailing _ to prevent name clashes.
	fn register_(pmod: Option<&str>, name: Option<&str>);
}

/// No sensible cleanup when we can't even allocate the exception message, just abort the process.
fn abort_in_pred<E: Display>(exc: E) -> ! {
	eprintln!("Could not create prolog exception, aborting. Err: {}", exc);
	std::process::abort();
}

/// Internal function, exposed only to simplify the prolog_predicate macro. Do not use.
///
/// Actually runs a (semi)det prolog predicate, taking care of setup and cleanup.
#[doc(hidden)]
pub unsafe fn internal_do_prolog_call<F>(cargs: &[term_t], func: F) -> foreign_t
where
	F: for<'a> FnOnce(&'a PredicateFrame, &mut [Option<Term<'a>>]) -> Result<(), Error>,
{
	let exc_term = PL_new_term_ref();
	let _engbind = global_state::bind(exc_term);

	let frame = PredicateFrame::new();
	let mut safe_terms: SmallVec<[Option<Term>; 16]> = SmallVec::new();
	for unsafe_term in cargs {
		safe_terms.push(Some(Term::take(*unsafe_term)));
	}

	let res = catch_unwind(AssertUnwindSafe(|| (func)(&frame, &mut safe_terms)));

	match res {
		Ok(Ok(())) => 1 as foreign_t,
		Ok(Err(Error::False)) => 0 as foreign_t,
		Ok(Err(Error::Exception(Exception))) => PL_raise_exception(exc_term) as foreign_t,
		Err(v) => create_panic_exception(&frame, v),
	}
}

/// Internal function, exposed only to simplify the prolog_predicate macro. Do not use.
///
/// Actually runs a nondet prolog predicate, taking care of setup and cleanup.
#[doc(hidden)]
pub unsafe fn internal_do_prolog_call_nondet<F, S>(
	cargs: &[term_t],
	ctrl: control_t,
	func: F,
) -> foreign_t
where
	F: for<'a> FnOnce(
		&mut Option<Box<S>>,
		&'a PredicateFrame,
		&mut [Option<Term<'a>>],
	) -> Result<bool, Error>,
{
	let exc_term = PL_new_term_ref();
	let _engbind = global_state::bind(exc_term);

	let call_type = PL_foreign_control(ctrl);
	let state_ptr = PL_foreign_context_address(ctrl);
	let mut state: Option<Box<S>> = if state_ptr.is_null() {
		None
	} else {
		Some(Box::from_raw(state_ptr as *mut S))
	};

	let frame = PredicateFrame::new();

	if call_type == PL_PRUNED {
		// Drop may panic
		return try_drop(&frame, state, || 1 as foreign_t);
	}

	let mut safe_terms: SmallVec<[Option<Term>; 16]> = SmallVec::new();
	for unsafe_term in cargs {
		safe_terms.push(Some(Term::take(*unsafe_term)));
	}

	let res = catch_unwind(AssertUnwindSafe(|| {
		(func)(&mut state, &frame, &mut safe_terms)
	}));

	match res {
		Ok(Ok(true)) => {
			return _PL_retry_address(
				state
					.map(|b| Box::into_raw(b) as *mut c_void)
					.unwrap_or(ptr::null_mut()),
			)
		}
		Ok(Ok(false)) => try_drop(&frame, state, || 1 as foreign_t),
		Ok(Err(Error::False)) => try_drop(&frame, state, || 0 as foreign_t),
		Ok(Err(Error::Exception(Exception))) => try_drop(&frame, state, move || {
			PL_raise_exception(global_state::get_exception()) as foreign_t
		}),
		Err(v) => try_drop(&frame, state, || create_panic_exception(&frame, v)),
	}
}

unsafe fn try_drop<F: Frame, T, OK: FnOnce() -> foreign_t>(frame: &F, v: T, ok: OK) -> foreign_t {
	if let Err(e) = catch_unwind(AssertUnwindSafe(move || std::mem::drop(v))) {
		return create_panic_exception(frame, e);
	} else {
		return (ok)();
	}
}

unsafe fn create_panic_exception<F: Frame>(
	frame: &F,
	v: Box<dyn std::any::Any + Send>,
) -> foreign_t {
	let t_err = frame.alloc();
	let functor = Functor::from_str_arity("rust_panic", 1);
	let t_msg = frame.alloc();

	if let Some(smsg) = v.downcast_ref::<String>() {
		t_msg
			.put_str(StringType::String, &smsg)
			.unwrap_or_else(|e| abort_in_pred(e));
	} else if let Some(smsg) = v.downcast_ref::<&'static str>() {
		t_msg
			.put_str(StringType::String, *smsg)
			.unwrap_or_else(|e| abort_in_pred(e));
	} else {
		t_msg
			.put_str(StringType::Atom, "non_string")
			.unwrap_or_else(|e| abort_in_pred(e));
	}
	t_err
		.cons_functor(frame, &functor, &[&t_msg])
		.unwrap_or_else(|e| abort_in_pred(e));
	PL_raise_exception(t_err.term) as foreign_t
}

/// Macro for defining a (semi)deterministic Prolog predicate in Rust.
///
/// The signature of the function must be `<'lt>(&'lt Frame [, Term<'lt> [, ...]]) [ "+-?09" ] -> Result<(), Error<'lt>>`.
/// The optional string literal in between the arguments and the `->` denotes the meta-predicate specification for the function -
/// see `meta_predicate` and `PL_register_foreign_in_module` for info and spec.
///
/// Returning `Ok(())` will result in the predicate returning true. `Err(Error::Fail)` will result in the predicate
/// returning false. `Err(Error::Exception(...))` will result in a Prolog exception.
///
/// Panics are caught, and will result in a Prolog exception - either `rust_panic("<msg>")` for panics with a `String`,
/// or `rust_panic(non_string)` for other objects.
///
/// Predicates defined in this way can be used in two ways: either via exporting from a C shared library via the `export_prolog_predicates`
/// or registering them manually by calling `mypredicate::register(modname, predname)` if embedding SWI-Prolog (`modname` and `predname` are
/// both `Option<&str>`s that specify the module and predicate names).
#[macro_export]
macro_rules! prolog_predicate {
	(
		$(#[$meta:meta])*
		$vis:vis fn $name:ident<$lt:lifetime>($framearg:ident : $framety:ty $(, $arg:ident : $argty:ty)* $(,)?) $($pmeta:literal)? -> $ret:ty $body:block
	) => {
		$(#[$meta])*
		#[allow(non_camel_case_types)]
		$vis struct $name;
		unsafe impl $crate::predicate_export::InternalPrologPredicate for $name {
			const IDENT_NAME: &'static str = stringify!($name);
			const NONDET: bool = false;
			const ARITY: i32 = $crate::prolog_predicate!(@count $([$arg])*);
			const META: ::core::option::Option<&'static str> = $crate::prolog_predicate!(@meta $($pmeta)?);
			fn predicate() -> unsafe extern "C" fn() -> $crate::sys::foreign_t {
				fn safe<$lt> ( $framearg : $framety, args: &mut [::core::option::Option<$crate::term::Term<$lt>>]) -> $ret {
					let mut _i = 0;
					$(
						let $arg : $argty = args[_i].take().unwrap();
						_i += 1;
					)*
					$body
				}

				unsafe extern "C" fn raw($($arg: $crate::sys::term_t),*) -> $crate::sys::foreign_t {
					let args = &[$($arg),*];
					$crate::predicate_export::internal_do_prolog_call(args, safe)
				}

				let raw = raw as *const ();
				unsafe { std::mem::transmute(raw) }
			}
			fn register_(pmod: Option<&str>, name: Option<&str>) {
				Self::register(pmod, name)
			}
		}
		impl $name {
			fn register(pmod: Option<&str>, name: Option<&str>) {
				unsafe {
					$crate::predicate_export::internal_export_prolog_predicate::<Self>(false, pmod, name)
				}
			}
		}
	};
	(@count [$($_tt:tt)*] $([$($tt:tt)*])*) => { 1 + $crate::prolog_predicate!(@count $([$($tt:tt)*])*) };
	(@count ) => { 0 };
	(@meta ) => { ::core::option::Option::None };
	(@meta $meta:literal) => { ::core::option::Option::Some($meta) };
}

/// Macro for defining a nondeterministic Prolog predicate in Rust.
///
/// This has similar usage to `prolog_nondet_predicate`, with the following differences:
///
/// * The function may be called more than once. Each call should generate a unique solution.
/// * An additional argument is prepended to the argument list: `state: &mut Option<Box<S>>` where `S` is an arbitrary type.
///   This state is persisted across calls. It initially starts as `None` - implementors should use `Option::get_or_insert_with` to
///   initialize the state if needed and get a reference to it.
/// * The function returns `Result<bool, Error>` instead of `Result<(), Error>`. `Ok(true)` means to succeed with a choice point - that
///   additional results are available. `Ok(false)` means to succeed without a choice point - that no more results are available. `Err`
///   results are handled the same.
///
/// This library automatically handles cuts by simply dropping the state box.
#[macro_export]
macro_rules! prolog_nondet_predicate {
	(
		$(#[$meta:meta])*
		$vis:vis fn $name:ident<$lt:lifetime>($statearg:ident : $statety:ty, $framearg:ident : $framety:ty $(, $arg:ident : $argty:ty)* $(,)?) $($pmeta:literal)? -> $ret:ty $body:block
	) => {
		$(#[$meta])*
		#[allow(non_camel_case_types)]
		$vis struct $name;
		unsafe impl $crate::predicate_export::InternalPrologPredicate for $name {
			const IDENT_NAME: &'static str = stringify!($name);
			const NONDET: bool = true;
			const ARITY: i32 = $crate::prolog_predicate!(@count $([$arg])*);
			const META: ::core::option::Option<&'static str> = $crate::prolog_predicate!(@meta $($pmeta)?);
			fn predicate() -> unsafe extern "C" fn() -> $crate::sys::foreign_t {
				fn safe<$lt> ( $statearg : $statety, $framearg : $framety, args: &mut [::core::option::Option<$crate::term::Term<$lt>>]) -> $ret {
					let mut _i = 0;
					$(
						let $arg : $argty = args[_i].take().unwrap();
						_i += 1;
					)*
					$body
				}

				unsafe extern "C" fn raw($($arg: $crate::sys::term_t,)* ctrl: $crate::sys::control_t) -> $crate::sys::foreign_t {
					let args = &[$($arg),*];
					$crate::predicate_export::internal_do_prolog_call_nondet(args, ctrl, safe)
				}

				let raw = raw as *const ();
				unsafe { std::mem::transmute(raw) }
			}
			fn register_(pmod: Option<&str>, name: Option<&str>) {
				Self::register(pmod, name)
			}
		}
		impl $name {
			fn register(pmod: Option<&str>, name: Option<&str>) {
				unsafe {
					$crate::predicate_export::internal_export_prolog_predicate::<Self>(false, pmod, name)
				}
			}
		}
	};
	(@count [$($_tt:tt)*] $([$($tt:tt)*])*) => { 1 + $crate::prolog_predicate!(@count $([$($tt:tt)*])*) };
	(@count ) => { 0 };
	(@meta ) => { ::core::option::Option::None };
	(@meta $meta:literal) => { ::core::option::Option::Some($meta) };
}

/// Exports a C function compatible with `use_foreign_library` that registers predicates.
///
/// Syntax for each export line is `[@"mod"] ["export_name" : ] predicate`, where:
///
/// * `mod` is the module to export to, defaulting to the current module,
/// * `export_name` is the name to expose the predicate under, defaulting to the original identifier for the function, and
/// * `predicate` is the Rust function to expose, defined with `prolog_predicate`.
///
/// Arity is automatically determined via how many arguments the predicate function takes. If you want to define a predicate with
/// multiple arities, you will need to use the `export_name` option to make them have the same name.
///
/// ```rust
/// use swi_prolog::prelude::*;
/// prolog_predicate!(pub fn mypred1<'a>(frame: &'a PredicateFrame) -> Result<(), PlError> { Ok(()) });
/// prolog_predicate!(pub fn mypred2<'a>(frame: &'a PredicateFrame, foo: Term<'a>) -> Result<(), PlError> { Ok(()) });
/// prolog_predicate!(pub fn mypred3<'a>(frame: &'a PredicateFrame, foo: Term<'a>, bar: Term<'a>) -> Result<(), PlError> { Ok(()) });
///
/// export_prolog_predicates!{
/// 	export = {
/// 		mypred1,
/// 		"my_pred_2": mypred2,
/// 		@"mymod" "my_pred_3": mypred3,
/// 	};
/// 	// Name of the C-facing install function, if needed. Should only be called by Prolog loader.
/// 	unsafe extern "C" fn install();
/// 	// Name of the Rust facing register function, if needed. Can be called by Rust before engine initialization.
/// 	fn register();
/// }
/// ```
#[macro_export]
macro_rules! export_prolog_predicates(
	(
		export = {
			$($exports:tt)*
		};
		$($rest:tt)*
	) => {
		$crate::export_prolog_predicates!(@parse_fn { $($exports)* } { $($exports)* } { $($rest)* });
	};

	// Dumb hack: trying to forward the exports naturally causes local ambiguity errors, so copy the unparsed tt as well every time.
	(@parse_fn { $( $( @ $pmod:literal )? $($name:literal : )? $func:path ),* $(,)? } { $($exports:tt)* } {}) => {};
	(@parse_fn { $( $( @ $pmod:literal )? $($name:literal : )? $func:path ),* $(,)? } { $($exports:tt)* } {
		unsafe extern "C" fn $loader_name:ident();
		$($rest:tt)*
	}) => {
		#[no_mangle]
		pub unsafe extern "C" fn $loader_name() {
			$(
				$crate::predicate_export::internal_export_prolog_predicate::<$func>(
					true,
					$crate::export_prolog_predicates!(@opt $($pmod)?),
					$crate::export_prolog_predicates!(@opt $($name)?),
				);
			)*
		}

		$crate::export_prolog_predicates!(@parse_fn { $($exports)* } { $($exports)* } {$($rest)*});
	};
	(@parse_fn { $( $( @ $pmod:literal )? $($name:literal : )? $func:path ),* $(,)? } { $($exports:tt)* } {
		fn $loader_name:ident();
		$($rest:tt)*
	}) => {
		pub fn $loader_name() {
			unsafe {
				$(
					$crate::predicate_export::internal_export_prolog_predicate::<$func>(
						false,
						$crate::export_prolog_predicates!(@opt $($pmod)?),
						$crate::export_prolog_predicates!(@opt $($name)?),
					);
				)*
			}
		}

		$crate::export_prolog_predicates!(@parse_fn { $($exports)* } { $($exports)* } {$($rest)*});
	};

	(@opt ) => { ::core::option::Option::None };
	(@opt $name:literal) => { ::core::option::Option::Some($name) };
);

/// No sensible cleanup when we can't register a function
fn abort_in_install<E: Display>(exc: E) -> ! {
	eprintln!("Could not register prolog function, aborting. Err: {}", exc);
	std::process::abort();
}

/// Internal function, exposed only to simplify the export_prolog_predicates macro. Do not use.
#[doc(hidden)]
pub unsafe fn internal_export_prolog_predicate<P: InternalPrologPredicate>(
	is_in_install: bool,
	modname: Option<&str>,
	prolog_name: Option<&str>,
) {
	if is_in_install {
		global_state::set_prolog_initted();
	}

	let prolog_name = prolog_name.unwrap_or(P::IDENT_NAME);
	let c_modname = modname.map(|s| CString::new(s).unwrap_or_else(|e| abort_in_install(e)));
	let c_name = CString::new(prolog_name).unwrap_or_else(|e| abort_in_install(e));
	let c_meta = P::META.map(|s| CString::new(s).unwrap_or_else(|e| abort_in_install(e)));

	let mut flags = 0;
	if P::NONDET {
		flags = flags | PL_FA_NONDETERMINISTIC;
	}

	if let Some(c_meta) = c_meta {
		flags = flags | PL_FA_META;
		if PL_register_foreign_in_module(
			c_modname
				.as_ref()
				.map(|s| s.as_ptr())
				.unwrap_or(ptr::null()),
			c_name.as_ptr(),
			P::ARITY,
			Some(P::predicate()),
			flags,
			c_meta.as_ptr(),
		) == 0
		{
			abort_in_install("registration failed");
		}
	} else {
		if PL_register_foreign_in_module(
			c_modname
				.as_ref()
				.map(|s| s.as_ptr())
				.unwrap_or(ptr::null()),
			c_name.as_ptr(),
			P::ARITY,
			Some(P::predicate()),
			flags,
		) == 0
		{
			abort_in_install("registration failed");
		}
	}
}

#[cfg(test)]
#[allow(unused_variables)]
mod test {
	use super::*;

	prolog_predicate!(
		pub fn mypred1<'a>(frame: &'a PredicateFrame) -> Result<(), Error> {
			Ok(())
		}
	);
	prolog_predicate!(
		pub fn mypred2<'a>(frame: &'a PredicateFrame, foo: Term<'a>) -> Result<(), Error> {
			Ok(())
		}
	);
	prolog_predicate!(
		pub fn mypred3<'a>(
			frame: &'a PredicateFrame,
			foo: Term<'a>,
			bar: Term<'a>,
		) -> Result<(), Error> {
			Ok(())
		}
	);

	export_prolog_predicates! {
		export = {
			mypred1,
			"my_pred_2": mypred2,
			@"mymod" "my_pred_3": mypred3,
		};
		unsafe extern "C" fn install_mylib();
		fn _register();
	}
}
