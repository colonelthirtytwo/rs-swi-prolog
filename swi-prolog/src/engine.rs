//! Prolog engine, entrypoint for embedding SWI-Prolog.

use std::{
	cell::Cell,
	ffi::CString,
	path::Path,
	ptr,
};

use scopeguard::defer;
use swi_prolog_sys::*;
use thiserror::Error;

use crate::{
	exception::Exception,
	frame::{
		Frame,
		FrameSealed,
	},
	global_state,
	module::Module,
	predicate::Predicate,
	term::Term,
};

/// Prolog execution engine.
///
/// Use this struct if you wish to embed SWI-Prolog into your program. It is responsible for initializing Prolog and making queries.
///
/// The first engine must be created via `Engine::initialize`, which does SWI-Prolog's global initialization and returns the "main" engine.
/// After that, you can create other engines via `Engine::new_sub_engine`, ex. for multithreading.
pub struct Engine {
	pub(crate) handle: PL_engine_t,
	has_query: Cell<bool>,
}
impl Engine {
	/// Initializes SWI-PL and returns the "main" engine.
	///
	/// Must be called before most other prolog functionality
	pub fn initialize(argv0: &str, args: &InitArgs) -> Result<Engine, CreateEngineError> {
		let res = global_state::try_prolog_init(|| unsafe {
			// Args strings and list must live as long as the main prolog engine, so just leak them.
			let mut arglist = vec![CString::new(argv0).unwrap()];
			args.append_args(&mut arglist);
			let argv = arglist
				.into_iter()
				.map(|s| Box::leak(s.into_boxed_c_str()).as_ptr() as *mut _)
				.collect::<Vec<_>>()
				.into_boxed_slice();
			let argv = Box::leak(argv);

			if let Some(blob) = args.qsave_program {
				if PL_set_resource_db_mem(blob.as_ptr(), blob.len()) == 0 {
					return Err(CreateEngineError::CreateFailed);
				}
			}

			if PL_initialise(argv.len() as i32, argv.as_mut_ptr()) == 0 {
				return Err(CreateEngineError::CreateFailed);
			}

			let mut current = ptr::null_mut();
			if PL_set_engine(ptr::null_mut(), &mut current as *mut _) != PL_ENGINE_SET {
				panic!("PL_set_engine failed");
			}
			return Ok(Self {
				handle: current,
				has_query: Cell::new(false),
			});
		});
		match res {
			Some(Ok(v)) => Ok(v),
			Some(Err(e)) => Err(e),
			None => Err(CreateEngineError::AlreadyInitialized),
		}
	}

	/// Creates a new non-main engine.
	///
	/// SWI-Prolog must have been initialized with `Engine::initialize` before this call, otherwise it will return an error.
	pub fn new_sub_engine() -> Result<Engine, CreateEngineError> {
		if !global_state::get_prolog_initted() {
			return Err(CreateEngineError::NotInitialized);
		}
		if global_state::is_bound() {
			return Err(CreateEngineError::EngineBound);
		}

		unsafe {
			let eng_ptr = PL_create_engine(ptr::null_mut());
			if eng_ptr.is_null() {
				return Err(CreateEngineError::CreateFailed);
			}
			Ok(Self {
				handle: eng_ptr,
				has_query: Cell::new(false),
			})
		}
	}

	/// Binds the engine to the current thread, allowing access to it.
	pub fn bind<R, F: FnOnce(&EngineFrame) -> R>(&self, func: F) -> R {
		if global_state::is_bound() {
			panic!("Tried to enter an SWIPL engine while already inside of one");
		}

		unsafe {
			if PL_set_engine(self.handle, ptr::null_mut()) != PL_ENGINE_SET {
				panic!("PL_set_engine failed");
			}
			defer!({
				if PL_set_engine(ptr::null_mut(), ptr::null_mut()) != PL_ENGINE_SET {
					panic!("PL_set_engine failed");
				}
			});

			let exc_term = PL_new_term_ref();
			let _engbind = global_state::bind(exc_term);

			let context = EngineFrame {
				eng: self,
				has_subframe: Cell::new(false),
			};
			(func)(&context)
		}
	}
}
impl std::ops::Drop for Engine {
	fn drop(&mut self) {
		assert!(
			unsafe { PL_destroy_engine(self.handle) } != 0,
			"PL_destroy_engine failed"
		);
	}
}

/// Frame specific to `Engine::bind`, extending it.
pub struct EngineFrame<'eng> {
	eng: &'eng Engine,
	has_subframe: Cell<bool>,
}
unsafe impl<'eng> Frame for EngineFrame<'eng> {
	fn close_ok(self)
	where
		Self: Sized,
	{
	}

	fn close_err(self)
	where
		Self: Sized,
	{
	}
}
unsafe impl<'eng> FrameSealed for EngineFrame<'eng> {
	unsafe fn get_has_subframe_cell(&self) -> &Cell<bool> {
		&self.has_subframe
	}
}
impl<'eng> EngineFrame<'eng> {
	/// Starts running a Prolog predicate
	///
	/// Only one query can be running on an engine at one time. Calling `query` again while
	/// an existing query hasn't been dropped will cause a panic.
	///
	/// If the length of the `args` array doesn't match the arity of the predicate, this will
	/// panic.
	pub fn query<'a, 't, I>(
		&'a self,
		ctx_mod: Option<&Module>,
		pred: &Predicate,
		args: I,
	) -> Query<'a, 'eng>
	where
		I: IntoIterator,
		I::Item: AsRef<Term<'t>>,
		I::IntoIter: ExactSizeIterator,
	{
		self.check_no_subframe();
		if self.eng.has_query.get() {
			panic!("Only one query can be active at a time");
		}

		let (_, arity, _) = pred.info();
		let qid = self
			.with_reset_frame(|frame| -> Result<_, ()> {
				unsafe {
					let (t_args, args_len) = frame.copy_contiguous(args);
					assert_eq!(
						args_len, arity as usize,
						"Length mismatch between predicate arity and arguments"
					);
					Ok(PL_open_query(
						ctx_mod.map(|m| m.0).unwrap_or(ptr::null_mut()),
						PL_Q_PASS_EXCEPTION | PL_Q_EXT_STATUS,
						pred.0,
						t_args,
					))
				}
			})
			.unwrap();
		if qid == 0 {
			panic!("PL_open_query failed");
		}
		self.eng.has_query.set(true);
		Query { frame: self, qid }
	}
}
impl<'eng> std::ops::Drop for EngineFrame<'eng> {
	fn drop(&mut self) {
		self.abort_no_subframe();
	}
}

/// Executing prolog query.
///
/// Only one of these at a time per engine.
///
/// Can be closed/cut manually with `Query::close` or `Query::cut`. Otherwise the query will try to close
/// the query on drop, but if an exception is thrown, it will cause a panic.
pub struct Query<'a, 'ctx> {
	frame: &'a EngineFrame<'ctx>,
	qid: qid_t,
}
impl<'a, 'ctx> Query<'a, 'ctx> {
	/// Advances the query
	pub fn next(&self) -> Result<QueryResult, Exception> {
		unsafe {
			match PL_next_solution(self.qid) {
				PL_S_EXCEPTION => Err(Exception::take_pl_exception_query(self.qid).expect(
					"PL_next_solution returned PL_S_EXCEPTION but no exception was present",
				)),
				PL_S_FALSE => Ok(QueryResult::False),
				PL_S_TRUE => Ok(QueryResult::True),
				PL_S_LAST => Ok(QueryResult::Last),
				v => panic!("Unrecognized result from PL_next_solution: {:?}", v),
			}
		}
	}

	/// Frees the query, keeping bindings
	pub fn cut(mut self) -> Result<(), Exception> {
		let qid = self.qid;
		self.qid = 0;
		unsafe {
			Exception::nonzero_or_exc(PL_cut_query(qid))?;
		}
		Ok(())
	}

	/// Frees the query, clearing bindings
	pub fn close(mut self) -> Result<(), Exception> {
		let qid = self.qid;
		self.qid = 0;
		unsafe {
			Exception::nonzero_or_exc(PL_close_query(qid))?;
		}
		Ok(())
	}
}
impl<'a, 'ctx> std::ops::Drop for Query<'a, 'ctx> {
	fn drop(&mut self) {
		if self.qid != 0 {
			unsafe {
				if PL_close_query(self.qid) == 0 {
					let exc = Exception::take_pl_exception();
					panic!("PL_close_query failed during Query drop: {:?}", exc);
				}
			}
		}
		debug_assert!(self.frame.eng.has_query.get());
		self.frame.eng.has_query.set(false);
	}
}

/// Result from `Query::next`.
#[derive(Debug, PartialEq, Eq, Hash, Clone, Copy)]
pub enum QueryResult {
	/// Query failed
	False,
	/// Query succeeded with choice point
	True,
	/// Query succeeded without choice point
	Last,
}
impl QueryResult {
	/// Returns true if the result value corresponds to one where the
	pub fn has_result(&self) -> bool {
		match self {
			Self::True | Self::Last => true,
			Self::False => false,
		}
	}
}

/// Prolog initialization arguments.
///
/// Used to create the arguments to pass to `PL_initialise`. These correspond to CLI arguments that `swipl` accepts.
pub struct InitArgs<'a> {
	/// Enable optimizations
	pub optimize: bool,
	/// Enable threading
	pub threads: bool,
	/// Explicit home location. Must be UTF-8 compliant
	pub home: Option<&'a Path>,
	/// Enable signal handling
	pub signals: bool,
	/// Use a particular signal for interruption
	pub sigalert: Option<u8>,
	/// Quiet
	pub quiet: bool,
	/// Prolog file to load
	pub file: Option<&'a Path>,

	/// If set, load blob as a qsave program via `PL_set_resource_db_mem`.
	///
	/// Must be `'static` since the prolog engine may load resources from this blob during execution.
	/// This isn't usually an issue if just using `include_bytes`, but if it's dynamic, use `Box::forget`.
	pub qsave_program: Option<&'static [u8]>,
}
impl<'a> Default for InitArgs<'a> {
	fn default() -> Self {
		Self {
			optimize: false,
			threads: true,
			home: None,
			signals: true,
			sigalert: None,
			quiet: true,
			file: None,
			qsave_program: None,
		}
	}
}
impl<'a> InitArgs<'a> {
	fn append_args(&self, args: &mut Vec<CString>) {
		if self.optimize {
			args.push(CString::new("-O").unwrap());
		}
		args.push(
			CString::new(if self.threads {
				"--threads"
			} else {
				"--no-threads"
			})
			.unwrap(),
		);
		if let Some(ref home) = self.home {
			// TODO: how to handle non-utf8 encoding?
			args.push(CString::new(format!("--home={}", home.to_str().unwrap())).unwrap());
		}
		args.push(
			CString::new(if self.signals {
				"--threads"
			} else {
				"--no-threads"
			})
			.unwrap(),
		);
		if let Some(sigalert) = self.sigalert {
			args.push(CString::new(format!("--sigalert={}", sigalert)).unwrap());
		}
		if self.quiet {
			args.push(CString::new("--quiet").unwrap());
		}
		if let Some(file) = self.file {
			args.push(CString::new(file.display().to_string()).unwrap());
		}
	}
}

#[derive(Debug, Error, Clone)]
pub enum CreateEngineError {
	#[error("Tried to initialize SWIPL twice")]
	AlreadyInitialized,
	#[error("Tried to create a non-main SWIPL engine without initialization")]
	NotInitialized,
	#[error("Tried to create an engine while another engine was bound.")]
	EngineBound,
	#[error("Creating the engine failed")]
	CreateFailed,
}
