//! Prolog-Rust data exchange using serde. Requires the `serde-fmt` feature.
//!
//! Lets you get and unify Rust types that implement `Serialize`/`Deserialize`.
//!
//! Mappings between serde's types and SWI-Prolog's:
//!
//! * All integer and float types correspond to Prolog integers and floats, respectively.
//!   Deserializing `u64`s is currently not implemented.
//! * `bool` translates to the atoms `true` and `false`.
//! * Strings translate to Prolog strings.
//! * Sequences translate to lists.
//! * Tuple structures and variants translate to functors. The struct/variant name becomes the functor tag.
//! * Maps are serialized as to SWI-Prolog dictionaries with no tags. Keys must be integer or string types.
//!   Maps cannot currently be deserialized, as SWI-PL does not have an API to iterate through map entries.
//! * Regular structures and variants are serialized as to SWI-Prolog dictionaries. The struct/variant name becomes the dict tag.
//!   Structures with named fields cannot currently be deserialized, as SWI-PL does not have an API to iterate through map entries.
//! * Tuples are currently not implemented.
//! * Some/None are currently not implemented, as SWI-Prolog does not have a standard "null" value. This may change.
//! * Bytes are not yet implemented, though you can serialize them as arrays of numbers.

mod de;
mod ser_unify;

use thiserror::Error;

use crate::{
	exception::Exception,
	term::TermType,
};

/// Error encountered during (de)serialization.
#[derive(Debug, Error)]
pub enum SerdeError {
	/// Some part of the structure failed to unify.
	/// Only applicable to `Term::unify_serde`.
	#[error("Unification failed")]
	UnifyFailed,
	/// A prolog exception occured while (de)serializing.
	#[error("Prolog exception: {0}")]
	PrologException(Exception),
	/// Encountered a map key that was not an integer or string.
	/// Only applicable to serialization.
	#[error("Invalid dict key type")]
	InvalidKey,
	/// Encountered a type that is not handled yet
	#[error("Cannot (de)serialize type: {0}")]
	InvalidType(&'static str),
	#[error("Expected {expected} but term had type {actual:?}")]
	UnexpectedType {
		expected: &'static str,
		actual: TermType,
	},
	/// Custom error
	#[error("{0}")]
	Custom(String),
}
impl serde::ser::Error for SerdeError {
	fn custom<T>(msg: T) -> Self
	where
		T: std::fmt::Display,
	{
		Self::Custom(msg.to_string())
	}
}
impl serde::de::Error for SerdeError {
	fn custom<T>(msg: T) -> Self
	where
		T: std::fmt::Display,
	{
		Self::Custom(msg.to_string())
	}
}
impl SerdeError {
	fn from_unify(v: Result<bool, Exception>) -> Result<(), Self> {
		match v {
			Ok(true) => Ok(()),
			Ok(false) => Err(Self::UnifyFailed),
			Err(exc) => Err(Self::PrologException(exc)),
		}
	}
}
impl From<Exception> for SerdeError {
	fn from(v: Exception) -> Self {
		Self::PrologException(v)
	}
}
