use serde::ser::{
	SerializeTuple,
	SerializeTupleStruct,
};

use crate::{
	prelude::*,
	serde::SerdeError,
};

impl<'ctx> Term<'ctx> {
	/// Unifies a term using serde.
	pub fn unify_serde<T: serde::Serialize, FR: Frame>(
		&self,
		frame: &'ctx FR,
		v: &T,
	) -> Result<(), SerdeError> {
		v.serialize(UnifySerializer { frame, term: self })
	}
}

struct UnifySerializer<'frame, 'term, FR: Frame> {
	frame: &'frame FR,
	term: &'term Term<'frame>,
}
impl<'frame, 'term, FR: Frame> serde::Serializer for UnifySerializer<'frame, 'term, FR> {
	type Error = SerdeError;
	type Ok = ();
	type SerializeMap = UnifySerDict<'frame, 'term, FR>;
	type SerializeSeq = UnifySerSeq<'frame, FR>;
	type SerializeStruct = UnifySerDict<'frame, 'term, FR>;
	type SerializeStructVariant = UnifySerDict<'frame, 'term, FR>;
	type SerializeTuple = serde::ser::Impossible<Self::Ok, Self::Error>;
	type SerializeTupleStruct = UnifySerTup<'frame, 'term, FR>;
	type SerializeTupleVariant = UnifySerTup<'frame, 'term, FR>;

	fn serialize_bool(self, v: bool) -> Result<Self::Ok, Self::Error> {
		SerdeError::from_unify(self.term.unify_bool(v))
	}

	fn serialize_i8(self, v: i8) -> Result<Self::Ok, Self::Error> {
		SerdeError::from_unify(self.term.unify_i64(v as i64))
	}

	fn serialize_i16(self, v: i16) -> Result<Self::Ok, Self::Error> {
		SerdeError::from_unify(self.term.unify_i64(v as i64))
	}

	fn serialize_i32(self, v: i32) -> Result<Self::Ok, Self::Error> {
		SerdeError::from_unify(self.term.unify_i64(v as i64))
	}

	fn serialize_i64(self, v: i64) -> Result<Self::Ok, Self::Error> {
		SerdeError::from_unify(self.term.unify_i64(v))
	}

	fn serialize_u8(self, v: u8) -> Result<Self::Ok, Self::Error> {
		SerdeError::from_unify(self.term.unify_u64(v as u64))
	}

	fn serialize_u16(self, v: u16) -> Result<Self::Ok, Self::Error> {
		SerdeError::from_unify(self.term.unify_u64(v as u64))
	}

	fn serialize_u32(self, v: u32) -> Result<Self::Ok, Self::Error> {
		SerdeError::from_unify(self.term.unify_u64(v as u64))
	}

	fn serialize_u64(self, v: u64) -> Result<Self::Ok, Self::Error> {
		SerdeError::from_unify(self.term.unify_u64(v))
	}

	fn serialize_f32(self, v: f32) -> Result<Self::Ok, Self::Error> {
		SerdeError::from_unify(self.term.unify_f64(v as f64))
	}

	fn serialize_f64(self, v: f64) -> Result<Self::Ok, Self::Error> {
		SerdeError::from_unify(self.term.unify_f64(v))
	}

	fn serialize_char(self, v: char) -> Result<Self::Ok, Self::Error> {
		// TODO: code point or str?
		SerdeError::from_unify(self.term.unify_u64(v as u64))
	}

	fn serialize_str(self, v: &str) -> Result<Self::Ok, Self::Error> {
		SerdeError::from_unify(self.term.unify_str(StringType::String, v))
	}

	fn serialize_bytes(self, _v: &[u8]) -> Result<Self::Ok, Self::Error> {
		Err(SerdeError::InvalidType("bytes"))
	}

	fn serialize_none(self) -> Result<Self::Ok, Self::Error> {
		Err(SerdeError::InvalidType("none"))
	}

	fn serialize_some<T: ?Sized>(self, _value: &T) -> Result<Self::Ok, Self::Error>
	where
		T: serde::Serialize,
	{
		Err(SerdeError::InvalidType("some"))
	}

	fn serialize_unit(self) -> Result<Self::Ok, Self::Error> {
		SerializeTuple::end(self.serialize_tuple(0)?)
	}

	fn serialize_unit_struct(self, name: &'static str) -> Result<Self::Ok, Self::Error> {
		SerializeTupleStruct::end(self.serialize_tuple_struct(name, 0)?)
	}

	fn serialize_unit_variant(
		self,
		_name: &'static str,
		_variant_index: u32,
		variant: &'static str,
	) -> Result<Self::Ok, Self::Error> {
		let atom = Atom::new(variant);
		Self::Error::from_unify(self.term.unify_atom(&atom))
	}

	fn serialize_newtype_struct<T: ?Sized>(
		self,
		name: &'static str,
		value: &T,
	) -> Result<Self::Ok, Self::Error>
	where
		T: serde::Serialize,
	{
		let mut s = self.serialize_tuple_struct(name, 1)?;
		s.serialize_field(value)?;
		SerializeTupleStruct::end(s)
	}

	fn serialize_newtype_variant<T: ?Sized>(
		self,
		_name: &'static str,
		_variant_index: u32,
		variant: &'static str,
		value: &T,
	) -> Result<Self::Ok, Self::Error>
	where
		T: serde::Serialize,
	{
		self.serialize_newtype_struct(variant, value)
	}

	fn serialize_seq(self, _len: Option<usize>) -> Result<Self::SerializeSeq, Self::Error> {
		let head = self.frame.alloc();
		let tail = self.frame.copy(self.term);
		Ok(UnifySerSeq {
			frame: self.frame,
			head,
			tail,
		})
	}

	fn serialize_tuple(self, _len: usize) -> Result<Self::SerializeTuple, Self::Error> {
		//let atom = Atom::new("");
		//UnifySerTup::new(self.frame, self.term, &atom, len)
		Err(SerdeError::InvalidType("tuple"))
	}

	fn serialize_tuple_struct(
		self,
		name: &'static str,
		len: usize,
	) -> Result<Self::SerializeTupleStruct, Self::Error> {
		let atom = Atom::new(name);
		UnifySerTup::new(self.frame, self.term, &atom, len)
	}

	fn serialize_tuple_variant(
		self,
		_name: &'static str,
		_variant_index: u32,
		variant: &'static str,
		len: usize,
	) -> Result<Self::SerializeTupleVariant, Self::Error> {
		let atom = Atom::new(variant);
		UnifySerTup::new(self.frame, self.term, &atom, len)
	}

	fn serialize_map(self, len: Option<usize>) -> Result<Self::SerializeMap, Self::Error> {
		Ok(UnifySerDict {
			frame: self.frame,
			uni_term: self.term,
			tag: None,
			keys: len.map(|l| Vec::with_capacity(l)).unwrap_or(Vec::new()),
			values: len.map(|l| Vec::with_capacity(l)).unwrap_or(Vec::new()),
		})
	}

	fn serialize_struct(
		self,
		name: &'static str,
		len: usize,
	) -> Result<Self::SerializeStruct, Self::Error> {
		Ok(UnifySerDict {
			frame: self.frame,
			uni_term: self.term,
			tag: Some(Atom::new(name)),
			keys: Vec::with_capacity(len),
			values: Vec::with_capacity(len),
		})
	}

	fn serialize_struct_variant(
		self,
		_name: &'static str,
		_variant_index: u32,
		variant: &'static str,
		len: usize,
	) -> Result<Self::SerializeStructVariant, Self::Error> {
		Ok(UnifySerDict {
			frame: self.frame,
			uni_term: self.term,
			tag: Some(Atom::new(variant)),
			keys: Vec::with_capacity(len),
			values: Vec::with_capacity(len),
		})
	}
}

struct UnifySerSeq<'frame, FR: Frame> {
	frame: &'frame FR,
	head: Term<'frame>,
	tail: Term<'frame>,
}
impl<'frame, FR: Frame> serde::ser::SerializeSeq for UnifySerSeq<'frame, FR> {
	type Error = SerdeError;
	type Ok = ();

	fn serialize_element<T: ?Sized>(&mut self, value: &T) -> Result<(), Self::Error>
	where
		T: serde::Serialize,
	{
		SerdeError::from_unify(self.tail.unify_list(&self.head, &self.tail))?;
		value.serialize(UnifySerializer {
			frame: self.frame,
			term: &self.head,
		})
	}

	fn end(self) -> Result<Self::Ok, Self::Error> {
		SerdeError::from_unify(self.tail.unify_nil())
	}
}

struct UnifySerTup<'frame, 'term, FR: Frame> {
	frame: &'frame FR,
	out_term: &'term Term<'frame>,
	arg_term: Term<'frame>,
	i: i32,
}
impl<'frame, 'term, FR: Frame> UnifySerTup<'frame, 'term, FR> {
	fn new(
		frame: &'frame FR,
		out_term: &'term Term<'frame>,
		name: &Atom,
		len: usize,
	) -> Result<Self, SerdeError> {
		let functor = Functor::new(name, len as u32);
		SerdeError::from_unify(out_term.unify_functor(&functor))?;
		Ok(Self {
			frame,
			out_term,
			arg_term: frame.alloc(),
			i: 1,
		})
	}
}
impl<'frame, 'term, FR: Frame> serde::ser::SerializeTuple for UnifySerTup<'frame, 'term, FR> {
	type Error = SerdeError;
	type Ok = ();

	fn serialize_element<T: ?Sized>(&mut self, value: &T) -> Result<(), Self::Error>
	where
		T: serde::Serialize,
	{
		self.arg_term.put_variable();
		value.serialize(UnifySerializer {
			frame: self.frame,
			term: &self.arg_term,
		})?;
		Self::Error::from_unify(self.out_term.unify_arg(self.i, &self.arg_term))?;
		self.i += 1;
		Ok(())
	}

	fn end(self) -> Result<Self::Ok, Self::Error> {
		Ok(())
	}
}
impl<'frame, 'term, FR: Frame> serde::ser::SerializeTupleStruct for UnifySerTup<'frame, 'term, FR> {
	type Error = SerdeError;
	type Ok = ();

	fn serialize_field<T: ?Sized>(&mut self, value: &T) -> Result<(), Self::Error>
	where
		T: serde::Serialize,
	{
		<Self as serde::ser::SerializeTuple>::serialize_element(self, value)
	}

	fn end(self) -> Result<Self::Ok, Self::Error> {
		<Self as serde::ser::SerializeTuple>::end(self)
	}
}
impl<'frame, 'term, FR: Frame> serde::ser::SerializeTupleVariant
	for UnifySerTup<'frame, 'term, FR>
{
	type Error = SerdeError;
	type Ok = ();

	fn serialize_field<T: ?Sized>(&mut self, value: &T) -> Result<(), Self::Error>
	where
		T: serde::Serialize,
	{
		<Self as serde::ser::SerializeTuple>::serialize_element(self, value)
	}

	fn end(self) -> Result<Self::Ok, Self::Error> {
		<Self as serde::ser::SerializeTuple>::end(self)
	}
}

struct UnifySerDict<'frame, 'term, FR: Frame> {
	frame: &'frame FR,
	uni_term: &'term Term<'frame>,
	tag: Option<Atom>,
	keys: Vec<Atom>,
	values: Vec<Term<'frame>>,
}
impl<'frame, 'term, FR: Frame> serde::ser::SerializeMap for UnifySerDict<'frame, 'term, FR> {
	type Error = SerdeError;
	type Ok = ();

	fn serialize_key<T: ?Sized>(&mut self, key: &T) -> Result<(), Self::Error>
	where
		T: serde::Serialize,
	{
		assert_eq!(
			self.keys.len(),
			self.values.len(),
			"serialize_key/serialize_value mismatch"
		);
		let k_atom = key.serialize(AtomSer)?;
		self.keys.push(k_atom);
		Ok(())
	}

	fn serialize_value<T: ?Sized>(&mut self, value: &T) -> Result<(), Self::Error>
	where
		T: serde::Serialize,
	{
		assert_eq!(
			self.keys.len(),
			self.values.len() + 1,
			"serialize_key/serialize_value mismatch"
		);
		let term = self.frame.alloc();
		value.serialize(UnifySerializer {
			frame: self.frame,
			term: &term,
		})?;
		self.values.push(term);
		Ok(())
	}

	fn end(self) -> Result<Self::Ok, Self::Error> {
		let put_term = self.frame.alloc();
		put_term.put_dict(
			self.frame,
			self.tag.as_ref(),
			self.keys.iter().zip(self.values.iter()),
		)?;
		SerdeError::from_unify(self.uni_term.unify_term(&put_term))
	}
}
impl<'frame, 'term, FR: Frame> serde::ser::SerializeStruct for UnifySerDict<'frame, 'term, FR> {
	type Error = SerdeError;
	type Ok = ();

	fn serialize_field<T: ?Sized>(
		&mut self,
		key: &'static str,
		value: &T,
	) -> Result<(), Self::Error>
	where
		T: serde::Serialize,
	{
		self.keys.push(Atom::new(key));
		let term = self.frame.alloc();
		value.serialize(UnifySerializer {
			frame: self.frame,
			term: &term,
		})?;
		self.values.push(term);
		Ok(())
	}

	fn end(self) -> Result<Self::Ok, Self::Error> {
		serde::ser::SerializeMap::end(self)
	}
}
impl<'frame, 'term, FR: Frame> serde::ser::SerializeStructVariant
	for UnifySerDict<'frame, 'term, FR>
{
	type Error = SerdeError;
	type Ok = ();

	fn serialize_field<T: ?Sized>(
		&mut self,
		key: &'static str,
		value: &T,
	) -> Result<(), Self::Error>
	where
		T: serde::Serialize,
	{
		serde::ser::SerializeStruct::serialize_field(self, key, value)
	}

	fn end(self) -> Result<Self::Ok, Self::Error> {
		serde::ser::SerializeStruct::end(self)
	}
}

/// Converts strings and integers to Atoms. Intended for dict keys.
struct AtomSer;
impl serde::ser::Serializer for AtomSer {
	type Error = SerdeError;
	type Ok = Atom;
	type SerializeMap = serde::ser::Impossible<Self::Ok, Self::Error>;
	type SerializeSeq = serde::ser::Impossible<Self::Ok, Self::Error>;
	type SerializeStruct = serde::ser::Impossible<Self::Ok, Self::Error>;
	type SerializeStructVariant = serde::ser::Impossible<Self::Ok, Self::Error>;
	type SerializeTuple = serde::ser::Impossible<Self::Ok, Self::Error>;
	type SerializeTupleStruct = serde::ser::Impossible<Self::Ok, Self::Error>;
	type SerializeTupleVariant = serde::ser::Impossible<Self::Ok, Self::Error>;

	fn serialize_bool(self, v: bool) -> Result<Self::Ok, Self::Error> {
		if v {
			Ok(Atom::new("true"))
		} else {
			Ok(Atom::new("false)"))
		}
	}

	fn serialize_i8(self, v: i8) -> Result<Self::Ok, Self::Error> {
		Ok(Atom::new(&v.to_string()))
	}

	fn serialize_i16(self, v: i16) -> Result<Self::Ok, Self::Error> {
		Ok(Atom::new(&v.to_string()))
	}

	fn serialize_i32(self, _v: i32) -> Result<Self::Ok, Self::Error> {
		todo!()
	}

	fn serialize_i64(self, v: i64) -> Result<Self::Ok, Self::Error> {
		Ok(Atom::new(&v.to_string()))
	}

	fn serialize_u8(self, v: u8) -> Result<Self::Ok, Self::Error> {
		Ok(Atom::new(&v.to_string()))
	}

	fn serialize_u16(self, v: u16) -> Result<Self::Ok, Self::Error> {
		Ok(Atom::new(&v.to_string()))
	}

	fn serialize_u32(self, v: u32) -> Result<Self::Ok, Self::Error> {
		Ok(Atom::new(&v.to_string()))
	}

	fn serialize_u64(self, v: u64) -> Result<Self::Ok, Self::Error> {
		Ok(Atom::new(&v.to_string()))
	}

	fn serialize_f32(self, _v: f32) -> Result<Self::Ok, Self::Error> {
		Err(SerdeError::InvalidKey)
	}

	fn serialize_f64(self, _v: f64) -> Result<Self::Ok, Self::Error> {
		Err(SerdeError::InvalidKey)
	}

	fn serialize_char(self, v: char) -> Result<Self::Ok, Self::Error> {
		Ok(Atom::new(&v.to_string()))
	}

	fn serialize_str(self, v: &str) -> Result<Self::Ok, Self::Error> {
		Ok(Atom::new(v))
	}

	fn serialize_bytes(self, _v: &[u8]) -> Result<Self::Ok, Self::Error> {
		Err(SerdeError::InvalidKey)
	}

	fn serialize_none(self) -> Result<Self::Ok, Self::Error> {
		Err(SerdeError::InvalidKey)
	}

	fn serialize_some<T: ?Sized>(self, value: &T) -> Result<Self::Ok, Self::Error>
	where
		T: serde::Serialize,
	{
		value.serialize(Self)
	}

	fn serialize_unit(self) -> Result<Self::Ok, Self::Error> {
		Err(SerdeError::InvalidKey)
	}

	fn serialize_unit_struct(self, _name: &'static str) -> Result<Self::Ok, Self::Error> {
		Err(SerdeError::InvalidKey)
	}

	fn serialize_unit_variant(
		self,
		_name: &'static str,
		_variant_index: u32,
		_variant: &'static str,
	) -> Result<Self::Ok, Self::Error> {
		Err(SerdeError::InvalidKey)
	}

	fn serialize_newtype_struct<T: ?Sized>(
		self,
		_name: &'static str,
		_value: &T,
	) -> Result<Self::Ok, Self::Error>
	where
		T: serde::Serialize,
	{
		Err(SerdeError::InvalidKey)
	}

	fn serialize_newtype_variant<T: ?Sized>(
		self,
		_name: &'static str,
		_variant_index: u32,
		_variant: &'static str,
		_value: &T,
	) -> Result<Self::Ok, Self::Error>
	where
		T: serde::Serialize,
	{
		Err(SerdeError::InvalidKey)
	}

	fn serialize_seq(self, _len: Option<usize>) -> Result<Self::SerializeSeq, Self::Error> {
		Err(SerdeError::InvalidKey)
	}

	fn serialize_tuple(self, _len: usize) -> Result<Self::SerializeTuple, Self::Error> {
		Err(SerdeError::InvalidKey)
	}

	fn serialize_tuple_struct(
		self,
		_name: &'static str,
		_len: usize,
	) -> Result<Self::SerializeTupleStruct, Self::Error> {
		Err(SerdeError::InvalidKey)
	}

	fn serialize_tuple_variant(
		self,
		_name: &'static str,
		_variant_index: u32,
		_variant: &'static str,
		_len: usize,
	) -> Result<Self::SerializeTupleVariant, Self::Error> {
		Err(SerdeError::InvalidKey)
	}

	fn serialize_map(self, _len: Option<usize>) -> Result<Self::SerializeMap, Self::Error> {
		Err(SerdeError::InvalidKey)
	}

	fn serialize_struct(
		self,
		_name: &'static str,
		_len: usize,
	) -> Result<Self::SerializeStruct, Self::Error> {
		Err(SerdeError::InvalidKey)
	}

	fn serialize_struct_variant(
		self,
		_name: &'static str,
		_variant_index: u32,
		_variant: &'static str,
		_len: usize,
	) -> Result<Self::SerializeStructVariant, Self::Error> {
		Err(SerdeError::InvalidKey)
	}
}
