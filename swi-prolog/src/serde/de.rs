use std::convert::TryFrom;

use serde::de::IntoDeserializer;

use super::SerdeError;
use crate::{
	frame::Frame,
	term::{
		ListType,
		Term,
		TermType,
	},
};

impl<'ctx> Term<'ctx> {
	/// Gets term data using serde.
	pub fn get_serde<T: serde::de::DeserializeOwned, F: Frame>(
		&self,
		frame: &F,
	) -> Result<T, SerdeError> {
		T::deserialize(Deser {
			frame,
			term: TermOrTermRef::Ref(self),
		})
	}
}

enum TermOrTermRef<'term, 'frame> {
	Owned(Term<'frame>),
	Ref(&'term Term<'frame>),
}
impl<'term, 'frame> std::ops::Deref for TermOrTermRef<'term, 'frame> {
	type Target = Term<'frame>;

	fn deref(&self) -> &Self::Target {
		match self {
			Self::Owned(ref t) => t,
			Self::Ref(t) => *t,
		}
	}
}

struct Deser<'frame, 'term, F: Frame> {
	frame: &'frame F,
	term: TermOrTermRef<'term, 'frame>,
}
impl<'frame, 'term, 'de, F: Frame> serde::de::Deserializer<'de> for Deser<'frame, 'term, F> {
	type Error = SerdeError;

	fn deserialize_any<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: serde::de::Visitor<'de>,
	{
		let ty = self.term.type_of();
		match ty {
			TermType::Atom => {
				let a = self.term.as_atom().unwrap();
				let s = a.as_str();
				if s == Some("true") {
					return visitor.visit_bool(true);
				}
				if s == Some("false") {
					return visitor.visit_bool(true);
				}
			}
			TermType::String => {
				let s = self.term.as_string().unwrap();
				return visitor.visit_string(s);
			}
			TermType::Integer => {
				let v = self.term.as_i64().unwrap();
				return visitor.visit_i64(v);
			}
			TermType::Rational | TermType::Float => {
				let v = self.term.as_f64().unwrap();
				return visitor.visit_f64(v);
			}
			TermType::ListPair | TermType::Nil => {
				return self.deserialize_seq(visitor);
			}
			TermType::Term => {
				if let Some(functor) = self.term.as_functor() {
					// TODO: is this the best visitor? Tag is lost.
					let arg_term = self.frame.alloc();
					return visitor.visit_seq(DeserFunctor {
						frame: self.frame,
						in_term: self.term,
						arg_term,
						i: 1,
						arity: functor.arity(),
					});
				}
				// TODO: what else could it be?
			}
			_ => {}
		};
		Err(SerdeError::InvalidType("any"))
	}

	fn deserialize_bool<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: serde::de::Visitor<'de>,
	{
		if let Some(atom) = self.term.as_atom() {
			let s = atom.as_str();
			if s == Some("true") {
				return visitor.visit_bool(true);
			}
			if s == Some("false") {
				return visitor.visit_bool(false);
			}
		}
		Err(SerdeError::UnexpectedType {
			expected: "bool",
			actual: self.term.type_of(),
		})
	}

	fn deserialize_i8<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: serde::de::Visitor<'de>,
	{
		if let Some(Ok(v)) = self.term.as_i64().map(i8::try_from) {
			return visitor.visit_i8(v);
		}
		Err(SerdeError::UnexpectedType {
			expected: "i8",
			actual: self.term.type_of(),
		})
	}

	fn deserialize_i16<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: serde::de::Visitor<'de>,
	{
		if let Some(Ok(v)) = self.term.as_i64().map(i16::try_from) {
			return visitor.visit_i16(v);
		}
		Err(SerdeError::UnexpectedType {
			expected: "i16",
			actual: self.term.type_of(),
		})
	}

	fn deserialize_i32<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: serde::de::Visitor<'de>,
	{
		if let Some(Ok(v)) = self.term.as_i64().map(i32::try_from) {
			return visitor.visit_i32(v);
		}
		Err(SerdeError::UnexpectedType {
			expected: "i32",
			actual: self.term.type_of(),
		})
	}

	fn deserialize_i64<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: serde::de::Visitor<'de>,
	{
		if let Some(v) = self.term.as_i64() {
			return visitor.visit_i64(v);
		}
		Err(SerdeError::UnexpectedType {
			expected: "i64",
			actual: self.term.type_of(),
		})
	}

	fn deserialize_u8<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: serde::de::Visitor<'de>,
	{
		if let Some(Ok(v)) = self.term.as_i64().map(u8::try_from) {
			return visitor.visit_u8(v);
		}
		Err(SerdeError::UnexpectedType {
			expected: "u8",
			actual: self.term.type_of(),
		})
	}

	fn deserialize_u16<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: serde::de::Visitor<'de>,
	{
		if let Some(Ok(v)) = self.term.as_i64().map(u16::try_from) {
			return visitor.visit_u16(v);
		}
		Err(SerdeError::UnexpectedType {
			expected: "u16",
			actual: self.term.type_of(),
		})
	}

	fn deserialize_u32<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: serde::de::Visitor<'de>,
	{
		if let Some(Ok(v)) = self.term.as_i64().map(u32::try_from) {
			return visitor.visit_u32(v);
		}
		Err(SerdeError::UnexpectedType {
			expected: "u32",
			actual: self.term.type_of(),
		})
	}

	fn deserialize_u64<V>(self, _visitor: V) -> Result<V::Value, Self::Error>
	where
		V: serde::de::Visitor<'de>,
	{
		Err(SerdeError::InvalidType("u64"))
	}

	fn deserialize_f32<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: serde::de::Visitor<'de>,
	{
		if let Some(v) = self.term.as_f64() {
			return visitor.visit_f32(v as f32);
		}
		Err(SerdeError::UnexpectedType {
			expected: "f32",
			actual: self.term.type_of(),
		})
	}

	fn deserialize_f64<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: serde::de::Visitor<'de>,
	{
		if let Some(v) = self.term.as_f64() {
			return visitor.visit_f64(v);
		}
		Err(SerdeError::UnexpectedType {
			expected: "f64",
			actual: self.term.type_of(),
		})
	}

	fn deserialize_char<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: serde::de::Visitor<'de>,
	{
		if let Some(v) = self.term.as_i64() {
			if let Ok(ch) = u32::try_from(v)
				.map_err(|_| ())
				.and_then(|v| char::try_from(v).map_err(|_| ()))
			{
				return visitor.visit_char(ch);
			}
		} else if let Some(s) = self.term.as_string() {
			let mut ch_iter = s.chars().fuse();
			let first_ch = ch_iter.next();
			let second_ch = ch_iter.next();
			if first_ch.is_some() && second_ch.is_none() {
				return visitor.visit_char(first_ch.unwrap());
			}
		}
		Err(SerdeError::UnexpectedType {
			expected: "char",
			actual: self.term.type_of(),
		})
	}

	fn deserialize_str<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: serde::de::Visitor<'de>,
	{
		if let Some(s) = self.term.as_string() {
			return visitor.visit_string(s);
		}
		Err(SerdeError::UnexpectedType {
			expected: "str",
			actual: self.term.type_of(),
		})
	}

	fn deserialize_string<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: serde::de::Visitor<'de>,
	{
		self.deserialize_str(visitor)
	}

	fn deserialize_bytes<V>(self, _visitor: V) -> Result<V::Value, Self::Error>
	where
		V: serde::de::Visitor<'de>,
	{
		Err(SerdeError::InvalidType("bytes"))
	}

	fn deserialize_byte_buf<V>(self, _visitor: V) -> Result<V::Value, Self::Error>
	where
		V: serde::de::Visitor<'de>,
	{
		Err(SerdeError::InvalidType("bytes"))
	}

	fn deserialize_option<V>(self, _visitor: V) -> Result<V::Value, Self::Error>
	where
		V: serde::de::Visitor<'de>,
	{
		Err(SerdeError::InvalidType("option"))
	}

	fn deserialize_unit<V>(self, _visitor: V) -> Result<V::Value, Self::Error>
	where
		V: serde::de::Visitor<'de>,
	{
		Err(SerdeError::InvalidType("unit"))
	}

	fn deserialize_unit_struct<V>(
		self,
		name: &'static str,
		visitor: V,
	) -> Result<V::Value, Self::Error>
	where
		V: serde::de::Visitor<'de>,
	{
		if let Some(functor) = self.term.as_functor() {
			if functor.name().as_str() == Some(name) && functor.arity() == 0 {
				return visitor.visit_unit();
			}
		}
		Err(SerdeError::UnexpectedType {
			expected: "unit_struct",
			actual: self.term.type_of(),
		})
	}

	fn deserialize_newtype_struct<V>(
		self,
		name: &'static str,
		visitor: V,
	) -> Result<V::Value, Self::Error>
	where
		V: serde::de::Visitor<'de>,
	{
		if let Some(functor) = self.term.as_functor() {
			if functor.name().as_str() == Some(name) && functor.arity() == 1 {
				let arg_term = self.frame.alloc();
				assert!(self.term.get_arg(1, &arg_term), "get_arg failed");
				return visitor.visit_newtype_struct(Deser {
					frame: self.frame,
					term: TermOrTermRef::Owned(arg_term),
				});
			}
		}
		Err(SerdeError::UnexpectedType {
			expected: "unit_struct",
			actual: self.term.type_of(),
		})
	}

	fn deserialize_seq<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: serde::de::Visitor<'de>,
	{
		let (ty, len) = self.term.skip_list(None)?;
		if ty == ListType::List {
			let head = self.frame.alloc();
			let tail = self.frame.copy(&self.term);
			return visitor.visit_seq(DeserList {
				frame: self.frame,
				head,
				tail,
				size_hint: Some(len),
			});
		}
		Err(SerdeError::UnexpectedType {
			expected: "list",
			actual: self.term.type_of(),
		})
	}

	fn deserialize_tuple<V>(self, _len: usize, _visitor: V) -> Result<V::Value, Self::Error>
	where
		V: serde::de::Visitor<'de>,
	{
		Err(SerdeError::InvalidType("tuple"))
	}

	fn deserialize_tuple_struct<V>(
		self,
		name: &'static str,
		len: usize,
		visitor: V,
	) -> Result<V::Value, Self::Error>
	where
		V: serde::de::Visitor<'de>,
	{
		if let Some(functor) = self.term.as_functor() {
			if functor.name().as_str() == Some(name) && functor.arity() == len {
				let arg_term = self.frame.alloc();
				return visitor.visit_seq(DeserFunctor {
					frame: self.frame,
					in_term: self.term,
					arg_term,
					i: 0,
					arity: len,
				});
			}
		}
		Err(SerdeError::UnexpectedType {
			expected: "functor",
			actual: self.term.type_of(),
		})
	}

	fn deserialize_map<V>(self, _visitor: V) -> Result<V::Value, Self::Error>
	where
		V: serde::de::Visitor<'de>,
	{
		Err(SerdeError::InvalidType("map"))
	}

	fn deserialize_struct<V>(
		self,
		_name: &'static str,
		_fields: &'static [&'static str],
		_visitor: V,
	) -> Result<V::Value, Self::Error>
	where
		V: serde::de::Visitor<'de>,
	{
		Err(SerdeError::InvalidType("struct"))
	}

	fn deserialize_enum<V>(
		self,
		_name: &'static str,
		_variants: &'static [&'static str],
		visitor: V,
	) -> Result<V::Value, Self::Error>
	where
		V: serde::de::Visitor<'de>,
	{
		visitor.visit_enum(DeserEnum {
			frame: self.frame,
			term: self.term,
		})
	}

	fn deserialize_identifier<V>(self, _visitor: V) -> Result<V::Value, Self::Error>
	where
		V: serde::de::Visitor<'de>,
	{
		// TODO: what is this method called for?
		Err(SerdeError::InvalidType("identifier"))
	}

	fn deserialize_ignored_any<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: serde::de::Visitor<'de>,
	{
		self.deserialize_any(visitor)
	}
}

struct DeserList<'frame, F: Frame> {
	frame: &'frame F,
	head: Term<'frame>,
	tail: Term<'frame>,
	size_hint: Option<usize>,
}
impl<'frame, 'de, F: Frame> serde::de::SeqAccess<'de> for DeserList<'frame, F> {
	type Error = SerdeError;

	fn next_element_seed<T>(&mut self, seed: T) -> Result<Option<T::Value>, Self::Error>
	where
		T: serde::de::DeserializeSeed<'de>,
	{
		if self.tail.is_nil() {
			return Ok(None);
		}
		assert!(
			self.tail.get_list(&self.head, &self.tail),
			"get_list failed"
		);
		self.size_hint.as_mut().map(|v| *v = *v - 1);
		seed.deserialize(Deser {
			frame: self.frame,
			term: TermOrTermRef::Ref(&self.head),
		})
		.map(Some)
	}

	fn size_hint(&self) -> Option<usize> {
		self.size_hint
	}
}

struct DeserFunctor<'frame, 'term, F: Frame> {
	frame: &'frame F,
	in_term: TermOrTermRef<'term, 'frame>,
	arg_term: Term<'frame>,
	i: usize,
	arity: usize,
}
impl<'frame, 'term, 'de, F: Frame> serde::de::SeqAccess<'de> for DeserFunctor<'frame, 'term, F> {
	type Error = SerdeError;

	fn next_element_seed<T>(&mut self, seed: T) -> Result<Option<T::Value>, Self::Error>
	where
		T: serde::de::DeserializeSeed<'de>,
	{
		if self.i == self.arity {
			return Ok(None);
		}
		assert!(self.in_term.get_arg(self.i + 1, &self.arg_term));
		self.i += 1;
		seed.deserialize(Deser {
			frame: self.frame,
			term: TermOrTermRef::Ref(&self.arg_term),
		})
		.map(Some)
	}
}

struct DeserEnum<'frame, 'term, F: Frame> {
	frame: &'frame F,
	term: TermOrTermRef<'term, 'frame>,
}
impl<'de, 'frame, 'term, F: Frame> serde::de::EnumAccess<'de> for DeserEnum<'frame, 'term, F> {
	type Error = SerdeError;
	type Variant = DeserVariant<'frame, 'term, F>;

	fn variant_seed<V>(self, seed: V) -> Result<(V::Value, Self::Variant), Self::Error>
	where
		V: serde::de::DeserializeSeed<'de>,
	{
		if let Some(atom) = self.term.as_atom() {
			if let Some(txt) = atom.as_str() {
				return seed.deserialize(txt.into_deserializer()).map(|v| {
					(
						v,
						DeserVariant {
							frame: self.frame,
							term: self.term,
							arity: 0,
						},
					)
				});
			}
		} else if let Some(functor) = self.term.as_functor() {
			if let Some(txt) = functor.name().as_str() {
				return seed.deserialize(txt.into_deserializer()).map(|v| {
					(
						v,
						DeserVariant {
							frame: self.frame,
							term: self.term,
							arity: functor.arity(),
						},
					)
				});
			}
		}
		Err(SerdeError::UnexpectedType {
			expected: "enum",
			actual: self.term.type_of(),
		})
	}
}

struct DeserVariant<'frame, 'term, F: Frame> {
	frame: &'frame F,
	term: TermOrTermRef<'term, 'frame>,
	arity: usize,
}
impl<'de, 'frame, 'term, F: Frame> serde::de::VariantAccess<'de>
	for DeserVariant<'frame, 'term, F>
{
	type Error = SerdeError;

	fn unit_variant(self) -> Result<(), Self::Error> {
		if self.arity == 0 {
			Ok(())
		} else {
			Err(SerdeError::InvalidType("unit variant"))
		}
	}

	fn newtype_variant_seed<T>(self, seed: T) -> Result<T::Value, Self::Error>
	where
		T: serde::de::DeserializeSeed<'de>,
	{
		if self.arity != 1 {
			return Err(SerdeError::InvalidType("newtype variant"));
		}
		let arg_term = self.frame.alloc();
		assert!(self.term.get_arg(1, &arg_term));
		seed.deserialize(Deser {
			frame: self.frame,
			term: TermOrTermRef::Owned(arg_term),
		})
	}

	fn tuple_variant<V>(self, len: usize, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: serde::de::Visitor<'de>,
	{
		if self.arity != len {
			return Err(SerdeError::InvalidType("tuple variant"));
		}
		let arg_term = self.frame.alloc();
		visitor.visit_seq(DeserFunctor {
			frame: self.frame,
			in_term: self.term,
			arg_term,
			i: 0,
			arity: self.arity,
		})
	}

	fn struct_variant<V>(
		self,
		_fields: &'static [&'static str],
		_visitor: V,
	) -> Result<V::Value, Self::Error>
	where
		V: serde::de::Visitor<'de>,
	{
		Err(SerdeError::InvalidType("struct variant"))
	}
}
