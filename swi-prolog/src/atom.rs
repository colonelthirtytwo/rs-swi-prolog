use std::{
	convert::TryInto,
	os::raw::c_char,
	slice,
};

use swi_prolog_sys::*;

/// Prolog Atom
///
/// These are refcounted and garbage collected.
#[repr(transparent)]
pub struct Atom(pub(crate) atom_t);
impl Atom {
	/// Wraps an atom_t, incrementing its ref count
	pub(crate) unsafe fn take(atom: atom_t) -> Self {
		PL_register_atom(atom);
		Self(atom)
	}

	/// Atom for nil, the empty list
	pub fn nil() -> Self {
		unsafe { Self(*_PL_atoms()) }
	}

	/// Atom for dot, the functor name for the list
	pub fn dot() -> Self {
		unsafe { Self(*(_PL_atoms().offset(1))) }
	}

	/// Creates an atom from a string.
	pub fn new(s: &str) -> Self {
		unsafe {
			Self(PL_new_atom_mbchars(
				REP_UTF8,
				s.len(),
				s.as_bytes().as_ptr() as *const c_char,
			))
		}
	}

	/*pub fn new_bytes(b: &[u8]) -> Self {
		unsafe {
			Self(PL_new_atom_mbchars(REP_MB, b.len() as u64, b.as_ptr() as *const c_char))
		}
	}*/

	/// Returns true if the atom is one of the exported atoms that are valid forever.
	fn is_exported(&self) -> bool {
		unsafe {
			let exported = _PL_atoms();
			self.0 == *exported || self.0 == *exported.offset(1)
		}
	}

	pub fn as_str(&self) -> Option<&str> {
		std::str::from_utf8(self.as_bytes()).ok()
	}

	pub fn as_bytes(&self) -> &[u8] {
		unsafe {
			let mut l = 0;
			let p = PL_atom_nchars(self.0, &mut l);
			let p = p as *const u8;
			slice::from_raw_parts(p, l as usize)
		}
	}
}
impl Clone for Atom {
	fn clone(&self) -> Self {
		unsafe {
			if !self.is_exported() {
				PL_register_atom(self.0);
			}
			Self(self.0)
		}
	}
}
impl std::ops::Drop for Atom {
	fn drop(&mut self) {
		unsafe {
			if !self.is_exported() {
				PL_unregister_atom(self.0);
			}
		}
	}
}
impl std::fmt::Display for Atom {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		if let Some(s) = self.as_str() {
			f.write_str(s)
		} else {
			write!(f, "{:?}", self.as_bytes())
		}
	}
}
impl std::fmt::Debug for Atom {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		if let Some(s) = self.as_str() {
			f.debug_tuple("Atom").field(&s).finish()
		} else {
			f.debug_tuple("Atom").field(&self.as_bytes()).finish()
		}
	}
}
impl AsRef<Atom> for Atom {
	fn as_ref(&self) -> &Atom {
		self
	}
}

/// Prolog functor - similar to an atom, arity tuple
#[derive(Clone)]
pub struct Functor(pub(crate) functor_t);
impl Functor {
	/// Creates a new functor from its name as an atom and its arity
	pub fn new(atom: &Atom, arity: u32) -> Self {
		let arity = arity.try_into().expect("Arity out of bounds");
		unsafe { Self(PL_new_functor(atom.0, arity)) }
	}

	/// Creates a new functor from its name as a string and its arity
	pub fn from_str_arity(name: &str, arity: u32) -> Self {
		let atom = Atom::new(name);
		Self::new(&atom, arity)
	}

	/// Gets the atom for the functor's name
	pub fn name(&self) -> Atom {
		unsafe { Atom::take(PL_functor_name(self.0)) }
	}

	/// Gets the functor's arity
	pub fn arity(&self) -> usize {
		unsafe { PL_functor_arity(self.0).try_into().unwrap() }
	}
}
impl std::fmt::Debug for Functor {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		let name = self.name();
		let arity = self.arity();
		f.debug_tuple("Functor").field(&name).field(&arity).finish()
	}
}
