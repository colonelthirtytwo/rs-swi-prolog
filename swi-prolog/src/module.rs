use swi_prolog_sys::*;

use crate::{
	atom::Atom,
	global_state,
};

/// Prolog Module
#[derive(Debug, PartialEq, Eq, Hash)]
#[repr(transparent)]
pub struct Module(pub(crate) module_t);
impl Module {
	/// Gets the module name atom
	pub fn name(&self) -> Atom {
		unsafe {
			let name = PL_module_name(self.0);
			Atom::take(name)
		}
	}

	/// Creates or gets a module
	pub fn new(name: &Atom) -> Self {
		global_state::check_prolog_initialized();
		unsafe {
			let modid = PL_new_module(name.0);
			Module(modid)
		}
	}
}
