use swi_prolog::{
	engine::InitArgs,
	prelude::*,
};

fn main() {
	let engine = Engine::initialize(
		"hello_world",
		&InitArgs {
			..Default::default()
		},
	)
	.expect("Init failed");

	engine.bind(|frame| {
		let pred = Predicate::get_by_name_arity("writeln", 1, None);
		let t_msg = frame.alloc();
		t_msg
			.put_str(StringType::String, "Hello World!")
			.expect("Could not put str");
		let q = frame.query(None, &pred, &[&t_msg]);
		while q.next().expect("Query exec failed").has_result() {
			println!("exec!");
		}
		q.close().expect("Close failed");
	});
}
