use swi_prolog::{
	engine::InitArgs,
	prelude::*,
};

fn main() {
	let engine = Engine::initialize(
		"exception",
		&InitArgs {
			..Default::default()
		},
	)
	.expect("Init failed");

	engine.bind(|frame| {
		let pred = Predicate::get_by_name_arity("throw", 1, Some("system"));
		let t_msg = frame.alloc();
		t_msg
			.put_str(StringType::String, "oops error")
			.expect("Could not put str");
		let q = frame.query(None, &pred, &[&t_msg]);

		let err = q.next().expect_err("throw succeeded?!");
		println!("We got an error that we expected: {}", err);
		q.close().expect("Close failed");
	});
}
