//! BEFORE BUILDING: run `swipl -g true -o main.qsave -c main.prolog` in this directory.
//! In a real project, you should do that in a `build.rs` script.

use swi_prolog::{
	engine::InitArgs,
	prelude::*,
	prolog_predicate,
};

prolog_predicate!(
	/// A custom predicate
	pub fn rust_predicate<'a>(_frame: &'a PredicateFrame, text: Term<'a>) -> Result<(), PlError> {
		if let Some(s) = text.as_string() {
			println!("{}", s);
			Ok(())
		} else {
			Err(PlError::False)
		}
	}
);

static PROGRAM: &[u8] = include_bytes!("main.qsave");

fn main() {
	rust_predicate::register(None, None);

	let engine = Engine::initialize(
		"embed_pl_state",
		&InitArgs {
			qsave_program: Some(PROGRAM),
			..Default::default()
		},
	)
	.expect("Init failed");

	engine.bind(|frame| {
		let pred = Predicate::get_by_name_arity("prolog_predicate", 1, None);
		let t_msg = frame.alloc();
		t_msg
			.put_str(StringType::String, "Hello World!")
			.expect("Could not put str");
		let q = frame.query(None, &pred, &[&t_msg]);
		while q.next().expect("Query exec failed").has_result() {
			println!("exec!");
		}
		q.close().expect("Close failed");
	});
}
