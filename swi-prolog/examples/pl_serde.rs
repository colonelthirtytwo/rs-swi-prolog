use serde::{
	Deserialize,
	Serialize,
};
use swi_prolog::{
	engine::InitArgs,
	prelude::*,
	serde::SerdeError,
};

#[derive(Serialize, Deserialize, Debug, PartialEq)]
#[serde(rename = "point")]
pub struct Point(f32, f32);

#[derive(Serialize, Debug, PartialEq)]
#[serde(rename = "rect")]
pub struct Rect {
	min: Point,
	max: Point,
}

fn main() {
	let engine = Engine::initialize(
		"serde_test",
		&InitArgs {
			..Default::default()
		},
	)
	.expect("Init failed");

	engine.bind(|frame| {
		// Point ser
		let t_point = frame.alloc();
		t_point
			.unify_serde(frame, &Point(1., 2.))
			.expect("ser failed");
		// Print the term. Should be `point(1.0,2.0)`
		println!("{}", t_point.format_canonical().expect("Format failed"));
		// Should unify the same point
		t_point
			.unify_serde(frame, &Point(1., 2.))
			.expect("Unification failed");
		// and not with a different point
		let err = t_point
			.unify_serde(frame, &Point(5., 6.))
			.expect_err("Expected unification failure");
		assert!(matches!(err, SerdeError::UnifyFailed));
		// Read it back
		assert_eq!(
			t_point.get_serde::<Point, _>(frame).expect("De failed"),
			Point(1., 2.)
		);

		// Rect ser
		let t_rect = frame.alloc();
		t_rect
			.unify_serde(
				frame,
				&Rect {
					min: Point(-1., -2.),
					max: Point(7., 5.),
				},
			)
			.expect("Ser failed");
		println!("{}", t_rect.format_canonical().expect("Format failed"));

		let t_msg = frame.alloc();
		t_msg
			.unify_serde(frame, &"Hello world!")
			.expect("Ser failed");
		println!("{}", t_msg.format_canonical().expect("Format failed"));
	});
}
