use swi_prolog::{
	engine::InitArgs,
	prelude::*,
};

fn main() {
	let engine = Engine::initialize(
		"subframe",
		&InitArgs {
			..Default::default()
		},
	)
	.expect("Init failed");

	engine.bind(|frame| {
		let term = frame.alloc();
		term.put_list().unwrap();

		frame
			.with_reset_frame(|inner| -> Result<(), Exception> {
				let list_head = inner.alloc();
				let list_tail = inner.alloc();
				assert!(term.unify_list(&list_head, &list_tail).unwrap());
				assert!(list_head.unify_i64(1234)?);
				Ok(())
			})
			.unwrap();
		let list_head = frame.alloc();
		assert!(term.get_list_head(&list_head));
		assert_eq!(list_head.as_i64(), Some(1234));

		let err = frame
			.with_reset_frame(|inner| -> Result<(), Exception> {
				let exc = inner.alloc();
				exc.put_str(StringType::String, "Error!")?;
				Err(Exception::put(exc))
			})
			.unwrap_err();
		println!("Got exception which was expected: {:?}", err);
	});
}
