use swi_prolog::{
	engine::InitArgs,
	prelude::*,
	prolog_predicate,
};

prolog_predicate!(
	/// A custom predicate
	pub fn hello_world<'a>(_frame: &'a PredicateFrame) -> Result<(), PlError> {
		println!("Hello world!");
		Ok(())
	}
);

fn main() {
	// Register our custom predicate. Can do this before initialization.
	hello_world::register(None, None);

	let engine = Engine::initialize(
		"hello_world",
		&InitArgs {
			..Default::default()
		},
	)
	.expect("Init failed");

	engine.bind(|frame| {
		// Get and run the predicate we made
		let pred = Predicate::get_by_name_arity("hello_world", 0, None);
		let q = frame.query(None, &pred, &[] as &[Term]);
		while q.next().expect("Query exec failed").has_result() {
			println!("exec!");
		}
		q.close().expect("Close failed");
	});
}
