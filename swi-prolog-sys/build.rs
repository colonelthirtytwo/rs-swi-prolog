use std::{
	env,
	path::PathBuf,
	process::{
		Command,
		Stdio,
	},
};

#[cfg(feature = "vendored")]
const VENDORED_CHECKOUT: &str = "V8.2.4";
#[cfg(feature = "vendored")]
const VENDORED_GIT_URL: &str = "https://github.com/SWI-Prolog/swipl.git";

#[cfg(not(feature = "vendored"))]
fn swipl_setup() -> PathBuf {
	let home = if let Some(s) = env::var_os("RS_SWI_PL_HOME") {
		PathBuf::from(s)
	} else {
		let out = Command::new("swipl")
			.arg("-g")
			.arg("current_prolog_flag(home, P),write(P),halt")
			.stdin(Stdio::null())
			.stdout(Stdio::piped())
			.stderr(Stdio::inherit())
			.output()
			.expect("Executing swipl failed. Set RS_SWI_PL_HOME env var instead.");
		PathBuf::from(String::from_utf8(out.stdout).unwrap())
	};

	let (libpath, libname) = if env::var_os("CARGO_CFG_WINDOWS").is_some() {
		(home.join("bin"), "libswipl")
	} else {
		let arch = env::var("CARGO_CFG_TARGET_ARCH").unwrap();
		let os = env::var("CARGO_CFG_TARGET_OS").unwrap();
		let component = format!("{}-{}", arch, os);
		(home.join("lib").join(component), "swipl")
	};

	println!("cargo:rustc-link-search=native={}", libpath.display());
	println!("cargo:rustc-link-lib=dylib={}", libname);

	home.join("include")
}

#[cfg(feature = "vendored")]
fn swipl_setup() -> PathBuf {
	let repo_dir = PathBuf::from(env::var_os("OUT_DIR").unwrap()).join("swipl");
	if !repo_dir.is_dir() {
		let git_status = Command::new("git")
			.arg("clone")
			.arg("--recurse-submodules")
			.arg("-b")
			.arg(VENDORED_CHECKOUT)
			.arg("--depth")
			.arg("1")
			.arg("--shallow-submodules")
			.arg("--")
			.arg(VENDORED_GIT_URL)
			.arg(&repo_dir)
			.stdin(Stdio::null())
			.spawn()
			.expect("Could not run git, is it installed?")
			.wait()
			.expect("Could not wait");
		if !git_status.success() {
			panic!("git clone failed");
		}
	}

	let cmake_dir = cmake::Config::new(&repo_dir)
		.define("SWIPL_SHARED_LIB", "OFF")
		.define("SWIPL_PACKAGES_JAVA", "OFF")
		.define("BUILD_TESTING", "OFF")
		.define("INSTALL_DOCUMENTATION", "OFF")
		.build();

	println!(
		"cargo:rustc-link-search=native={}",
		cmake_dir.join("build/src").display()
	);
	println!("cargo:rustc-link-lib=static=swipl");

	cmake_dir.join("build/home/include")
}

fn main() {
	println!("cargo:rerun-if-env-changed=RS_SWI_PL_HOME");

	let include = swipl_setup();

	println!("cargo:rerun-if-changed=wrapper.h");
	let bindings = bindgen::Builder::default()
		.header("wrapper.h")
		.whitelist_function("PL_.*")
		.whitelist_function("_PL_.*")
		.whitelist_type("PL_.*")
		.whitelist_var("PL_.*")
		.whitelist_var("BUF_.*")
		.whitelist_var("CVT_.*")
		.whitelist_var("REP_.*")
		.whitelist_var("Suser_.*")
		.whitelist_var("Scurrent_.*")
		.size_t_is_usize(true)
		.use_core()
		.generate_comments(false)
		.default_macro_constant_type(bindgen::MacroTypeVariation::Signed)
		.parse_callbacks(Box::new(bindgen::CargoCallbacks))
		.clang_arg(format!("-I{}", include.display()))
		.generate()
		.expect("Could not generate bindings");
	let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());
	bindings
		.write_to_file(out_path.join("bindings.rs"))
		.expect("Could not write bindings");
}
