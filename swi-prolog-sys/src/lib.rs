//! Bindgen-created bindings to SWI-Prolog
#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]

include!(concat!(env!("OUT_DIR"), "/bindings.rs"));

pub const PL_ENGINE_MAIN: PL_engine_t = 0x1 as PL_engine_t;
pub const PL_ENGINE_CURRENT: PL_engine_t = 0x2 as PL_engine_t;
pub type fid_t = PL_fid_t;
