
:- module(swi_prolog_example_lib, [
	my_prolog_test/0,
	times_two/2,
	do_rust_panic/0,
	range/2
]).
:- use_foreign_library('../target/debug/libswi_prolog_example_lib.so').
