//! Example of building a C library exporting predicate for SWI-Prolog

use swi_prolog::{
	export_prolog_predicates,
	prelude::*,
	prolog_nondet_predicate,
	prolog_predicate,
};

prolog_predicate! {
	/// Example predicate that just writes to stdout and returns true.
	pub fn my_prolog_test<'ctx>(_frame: &PredicateFrame) -> Result<(), PlError> {
		println!("my_prolog_test was called ok!");
		Ok(())
	}
}

prolog_predicate! {
	/// True if `a = b * 2`. Demonstrates term usasge.
	pub fn times_two<'ctx>(frame: &'ctx PredicateFrame, a: Term<'ctx>, b: Term<'ctx>) -> Result<(), PlError> {
		if b.is_variable() {
			let v = a.as_i64()
				.ok_or_else(|| frame.type_error("integer", &a))?;
			let out_v = v * 2;
			if !b.unify_i64(out_v)? {
				return Err(PlError::False);
			}
			return Ok(());
		} else {
			let v = b.as_i64()
				.ok_or_else(|| frame.type_error("integer", &a))?;
			if v % 2 == 1 {
				return Err(PlError::False);
			}
			let out_v = v/2;
			if !a.unify_i64(out_v)? {
				return Err(PlError::False);
			}
			return Ok(());
		}
	}
}

prolog_predicate! {
	/// Does a rust panic. Hopefully the prolog bindings catch it!
	pub fn do_rust_panic<'ctx>(_frame: &'ctx PredicateFrame) -> Result<(), PlError> {
		panic!("Oh no, a rust panic!")
	}
}

prolog_nondet_predicate! {
	/// A nondeterministic predicate.
	pub fn range<'ctx>(state: &mut Option<Box<RangeState>>, frame: &'ctx PredicateFrame, max: Term<'ctx>, out: Term<'ctx>) -> Result<bool, PlError> {
		let max = max.as_i64()
			.ok_or_else(|| frame.type_error("integer", &max))?;
		let state = state.get_or_insert_with(|| Box::new(RangeState {
			max,
			current: 0,
		}));

		if state.current >= state.max {
			Err(PlError::False)
		} else {
			out.unify_i64(state.current)?;
			state.current = state.current + 1;
			Ok(state.current < state.max)
		}
	}
}

/// State for `range` predicate
struct RangeState {
	max: i64,
	current: i64,
}
impl std::ops::Drop for RangeState {
	fn drop(&mut self) {
		println!("Range state was dropped!");
	}
}

export_prolog_predicates! {
	export = {
		my_prolog_test,
		times_two,
		do_rust_panic,
		range,
	};
	unsafe extern "C" fn install_libswi_prolog_example_lib();
	fn register();
}
